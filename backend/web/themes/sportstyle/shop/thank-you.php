<div class="container">
    <div class="clear50"></div>
    <!-- cart -->
    <div class="cart-panel text-center">
        <div class="title">Złożono zamówienie
            <?php if($order_id): ?>
            #<?= $order_id; ?>
            <?php endif; ?>
        </div>
        <div class="clear20"></div>
        <strong>Dziękujemy za złożenie zamówienia.</strong>
        <div class="clear20"></div>
    </div>
    <div class="clear50"></div>
</div>