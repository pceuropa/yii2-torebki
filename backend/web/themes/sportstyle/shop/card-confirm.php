<div class="container">

    <!-- cart -->
    <div class="cart-panel">
        <div class="title">Twój koszyk</div>

        <table class="table">
            <thead>
            <tr>
                <th>Produkt</th>
                <th></th>
                <th>Cena</th>
                <th>Ilość</th>
                <th>Razem</th>
            </tr>
            </thead>
            <tbody>
            <?php use common\models\LoginForm;

            foreach ($basket->getItems() as $k => $item): ?>
                <tr>
                    <th scope="row"><?= $item['product']->img(['width' => 64]); ?></th>
                    <td><?= $item['product']->name; ?>
                        <div class="product-category"><?= $item['product']->category->title; ?></div>
                    </td>
                    <td><?= $item['product']->priceBrutto(); ?> PLN</td>
                    <td>
                        <?= $item['attributes']['quantity']; ?>
                    </td>
                    <td><b><?= $item['product']->priceBrutto() * $item['attributes']['quantity']; ?> PLN</b></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="gray-box">
            <div class="row">
                <div class="col-md-5 delivery">
                    Sposób wysyłki
                    <div class="images">
                        <?php if ($basket->getTransportId() == 7): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/kurier.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Kurier DPD</div>
                            </a>
                        <?php endif; ?>
                        <?php if ($basket->getTransportId() == 10): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/odbior_osobisty.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Odbiór osobisty Gliwice</div>
                            </a>
                        <?php endif; ?>
                        <?php if ($basket->getTransportId() == 11): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/odbior_osobisty.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Odbiór osobisty Białystok</div>
                            </a>
                        <?php endif; ?>
                        <?php if ($basket->getTransportId() == 6): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/kurier_pobranie.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Kurier DPD za pobraniem</div>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-5 ">
                    Sposób płatności
                    <div class="images">
                        <?php if ($basket->getPaymentId() == 'przelew'): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/przelew.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Przelew</div>
                            </a>
                        <?php endif; ?>
                        <?php if ($basket->getPaymentId() == 'online'): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/credit_card.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Płatność online</div>
                            </a>
                        <?php endif; ?>
                        <?php if ($basket->getPaymentId() == 'odbior'): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/transfer.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Płatność przy odbiorze</div>
                            </a>
                        <?php endif; ?>
                        <?php if ($basket->getPaymentId() == 'raty'): ?>
                            <a href="#">
                                <div class="image-container active">
                                    <img src="/img/zagiel.png" width="80px" class="img-responsive">
                                </div>
                                <div class="caption">Raty Żagiel</div>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-2">
                    Koszt wysyłki
                    <div class="delivery-price"><?= $basket->getTransportCost(); ?> zł</div>
                </div>
            </div>
        </div>

        <div class="nheader">Podsumowanie</div>

        <div class="row">
            <div class="col-md-7">
                <div class="gray-box">
                    <b>Twoje uwagi do zamówienia:</b>
                    <div><?= $basket->getNotice(); ?></div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="gray-box">
                    <b>Kwota do zapłaty:</b>
                    <span class="sum-price"><?= $basket->toPay(); ?> zł</span><br><br>

                    <div class="row">
                        <div class="col-md-2">
                            <img src="/img/warning_icon.png" width="32" class="img-responsive"/>
                        </div>
                        <div class="col-md-8">
                            <span style="color: #f14837; font-size: 13px;"><b>Dokonaj zakupu na kwote 2500,00 zł a otrzymasz przesyłkę gratis!</b></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (Yii::$app->user->isGuest): ?>
            <?= $this->render('//site/login', ['model' => new LoginForm()]); ?>
        <?php else: ?>
            <div class="clear50"></div>
            <div class="row">
                <div class="col-md-6">
                    <a href="/" class="btn btn-default">Kontynuuj zakupy</a>
                </div>
                <div class="col-md-6" style="text-align: right;">
                    <div class="form-inline discount-code">
                        <a href="/shop/order" type="button" class="btn btn-primary">Zamówienie z obowiązkiem zapłaty</a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="clear50"></div>
    </div>

</div>
