<?php 
use frontend\modules\cms\models\Box;
use yii\helpers\Html;
?>
<div class="container start-box">
    <div class="row">
        <?php for ($i = 1; $i <= 4; $i++): ?>
            <div class="col-md-3 col-sm-6 col-xs-12 static-box-rwd">
                <?php $box = Box::forceGet('home' . $i, 100) ?>
                <div class="box box-<?php echo $i; ?>">
                    <figure>
                        <?php if ($box['link']): ?><a href="<?= $box['link']; ?>"><?php endif; ?>
                            <h4><?php echo $box['title'];?></h4>
                            <?php echo Html::img($baseUrl . '/images/smallbaner' . $i . '.png', ['class' => 'img-responsive', 'alt' => $box['title']]); ?>
                            <?php echo Html::a('<i class="fa fa-chevron-right" aria-hidden="true"></i>', $box['link'], ['class' => 'btn btn-success']); ?>
                            <?php if ($box['link']): ?></a><?php endif; ?>
                    </figure>
                    <div class="desc">
                        <?= $box['content']; ?>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>