<?php
/* @var $this View */
/* @var $content string */

use bizley\cookiemonster\CookieMonster;
use common\models\Category;
use common\models\Product;
use common\models\ProductSearch;
use common\widgets\Alert;
use frontend\themes\sportstyle\assets\AppAsset;
use frontend\models\Basket;
use frontend\modules\cms\models\Banner;
use frontend\modules\cms\models\Box;
use frontend\modules\cms\models\Page;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$isHome = isset(Yii::$app->params['home']) ? true : false;
$baseUrl = $this->baseUrl;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900&amp;subset=latin-ext" rel="stylesheet">       
        <meta name="google-site-verification" content="z_IjrKFw3PGvsWQsAP6MLDNpqkH0-mBkKx3OCsW6gmY"/>
        <meta name="google-site-verification" content="t2pFXmSimOABNTUO6N_J-44uXHjRgwcsJ9y9E9Rdfn0" />

        <?php $this->head() ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-77755729-1', 'auto');
            ga('send', 'pageview');
        </script>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <header>
            <div class="text-center top-address hidden"> <?= Box::forceGet('top-address')->content; ?></div>
            <nav>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="/"><?php echo Html::img($baseUrl . '/images/logo.png', ['class' => 'img-responsive logo']); ?></a>
                        </div>
                        <div class="col-md-4">

                            <form id="searchBar" class="search-bar-wrapper" method="get" action="/szukaj">
                                <div class="search-bar">
                                    <div class="search-text">
                                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                                        <input type="text" autocomplete="off" value="" name="ProductSearch[name]" placeholder="Szukana fraza">
                                    </div>
                                    <div class="search-groups visible-lg visible-md">
                                        <select name="ProductSearch[category_id]">
                                            <option value="">wszystkie</option>
                                            <?php foreach (Category::getMain() as $category): ?>
                                                <option value="<?= $category->id; ?>"><?= $category->title; ?></option>
                                                <?php $childs = $category->getChildLimit(21)->all(); ?>
                                                <?php foreach ($childs as $child): ?>
                                                    <option value="<?= $child->id; ?>"> -- <?= $child->title; ?></option>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="search-button">
                                        <button class="btn btn-success" type="submit">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="col-md-4">
                            <div class="bars">
                                <div class="phone menu-bars ">
                                    + 48 32 270 24 15<br>
                                    <a href="/pl/page/kontakt">Napisz do nas</a>
                                </div>
                                <div class="user text-center menu-bars">
                                    <a href="/client/profile">
                                        <?= Html::img('@web/images/user.png', ['class' => 'img-responsive']); ?>
                                        <span>Twój profil</span>
                                        <?php if (!Yii::$app->user->isGuest): ?>
                                            <?= Yii::$app->user->identity->toString ?>
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div class="cart text-center menu-bars">
                                    <a href="/shop/card" id="basket-ico">
                                        <?= Html::img('@web/images/cart.png', ['class' => 'img-responsive']); ?>
                                        <?php if ($count = Basket::scount()): ?>
                                            <span class="badge badge-danger"><?= $count; ?></span>
                                        <?php endif; ?>
                                        <span>Koszyk</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container">
                <?= Alert::widget() ?>
            </div>
            <div class="header <?= !$isHome ? 'header-small' : ''; ?>">
                <div class="navbar yamm">
                    <div class="container">
                        <div class="navbar-header visible-xs visible-sm">
                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar-collapse-1" class="navbar-collapse collapse padding-0">
                            <ul class="nav navbar-nav">
                                <?php foreach (Category::getMain() as $category): ?>
                                    <li class="dropdown">
                                        <a href="<?= $category->getLink(); ?>" class="dropdown-toggle js-activated" data-toggle="dropdown" aria-expanded="false"><?= $category->title; ?> <i class="fa fa-chevron-down" aria-hidden="true"></i> </a>
                                        <ul class="dropdown-menu sub-menu">
                                            <li>
                                                <div class="yamm-content">
                                                    <div class="row">
                                                        <?php $categoriesChunk = array_chunk($category->getChildLimit(14)->all(), 7); ?>
                                                        <?php foreach ($categoriesChunk as $categories): ?>
                                                            <div class="col-sm-4">
                                                                <?php foreach ($categories as $subcategory): ?>
                                                                    <?= Html::a($subcategory->title, $subcategory->getLink(), ['class' => 'subcategory-menu']); ?>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        <?php endforeach; ?>
                                                        <?php if ($promoInMenu = Product::promoMenu()): ?>
                                                            <div class="col-sm-4 left-border visible-md visible-lg">
                                                                <div class="hot-shot">GORĄCY STRZAŁ</div>
                                                                <a href="<?= $promoInMenu->url(); ?>" class="promo-in-menu">
                                                                    <img src="<?= $promoInMenu->getUrl('250x250'); ?>" class="img-responsive"/>

                                                                    <span><?= $promoInMenu->name; ?></span>

                                                                    <?php if ($promoInMenu->isPromo()): ?>
                                                                        <div class="old-price"><?= $promoInMenu->basicPriceBrutto(); ?> PLN</div>
                                                                    <?php endif; ?>
                                                                    <div class="price"><?= $promoInMenu->priceBrutto(); ?> PLN</div>
                                                                    <a class="button" href="/shop/add?id=<?= $promoInMenu->id; ?>">Do koszyka</a>
                                                                </a>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php if ($isHome): ?>
                    <?php
                    $this->registerCssFile('@web/css/owl.carousel.min.css');
                    $this->registerCssFile('@web/css/owl.theme.default.min.css');

                    $this->registerJsFile('@web/js/owl.carousel.min.js', ['depends' => [JqueryAsset::className()]]);
                    $this->registerJsFile('@web/js/app_start.js', ['depends' => [JqueryAsset::className()]]);
                    ?>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="owl-carousel owl-carousel-big">
                                    <?php foreach (Banner::getList() as $banner): ?>
                                        <div class="banner-container">
                                            <a href="<?= $banner['link']; ?>">
                                                <img src="/banner/<?= $banner['photo']; ?>" class="img-responsive"/>
                                                <button class="btn btn-success">Sprawdź</button>
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-sm hidden-xs">
                                <div class="owl-carousel-mini-container">
                                    <h3><?php echo Yii::t('app', 'Najcześciej kupowane'); ?></h3>
                                    <div class="owl-carousel owl-carousel-mini">


                                        <?php foreach (ProductSearch::byPromotion(5) as $row): ?>
                                            <div>                                                   
                                                <?= $this->render('//product/_slider', ['model' => $row]); ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->render('_four_boxes', ['baseUrl' => $baseUrl]); ?>
                    <div class="clearfix"></div>
                <?php endif; ?>
            </div>
        </header>
        <div class="clearfix"></div>
        <div class="container">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
            <div class="clear50"></div>

            <?= $content ?>
        </div>

        <section class="about">
            <?php $box = Box::forceGet('about'); ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="img-noborder">
                            <?= $box['content']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php //echo $this->render('_logos');?>

        <div class="newsletter">
            <div class="container">
                <div class="row" style="padding-top: 40px; padding-bottom: 40px;">
                    <div class="col-sm-4">
                        <form action="/newsletter/subscribe" method="post">
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                            <h4 class="beauty">Newsletter</h4>
                            <hr class="header-delimiter"/>
                            Dowiedz się o różnych ofertach i promocjach!
                            <div class="input-group">
                                <input type="text" class="form-control email-input" placeholder="Adres e-mail" name="NewsletterEmail[mail]"/>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary email-button" type="submit">Wyślij!</button>
                                </span>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="NewsletterEmail[terms]">Akceptuję regulamin</label>
                            </div>
                            <?php /* <div class="form-inline">
                              <div class="radio">
                              <label><input type="radio" class="email-radio" name="optradio" checked name="NewsletterEmail[action]">Zapisz się</label>
                              </div>
                              <div class="radio">
                              <label><input type="radio" class="email-radio" name="optradio" name="NewsletterEmail[action]">Zrezygnuj</label>
                              </div>
                              </div> */ ?>
                        </form>
                    </div>
                    <div class="col-sm-5" style="width: 100px;"></div>
                    <div class="col-sm-6">
                        <h4 class="beauty">Nasze atuty</h4>
                        <hr class="header-delimiter"/>
                        <div class="col-sm-4">
                            <img src="<?php echo $baseUrl; ?>/images/fast_delivery.png"/>
                            <b style="display: block">Szybka dostawa</b>
                        </div>
                        <div class="col-sm-4">
                            <img src="<?php echo $baseUrl; ?>/images/calendar.png"/>
                            <b style="display: block">14 dni na zwrot</b>
                        </div>
                        <div class="col-sm-4">
                            <img src="<?php echo $baseUrl; ?>/images/secure_payments.png"/>
                            <b style="display: block">Bezpieczne płatności</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="image-bar">
                <img src="/img/layer_20.png" class="img-responsive"/>
            </div>
        </div>

        <div class="line"></div>

        <div class="container">
            <!-- footer -->
            <div class="footer" style="padding-bottom: 20px; padding-top: 50px;">
                <div class="row">
                    <?php foreach (Page::getParents() as $parentPage): ?>
                        <div class="col-lg-2">
                            <h3><?= $parentPage->title; ?></h3>
                            <?php foreach ($parentPage->getChilds()->all() as $page): ?>
                                <?= Html::a($page->title, $page->getLink()); ?>
                            <?php endforeach; ?>
                        </div>

                    <?php endforeach; ?>
                    <div class="col-lg-2">
                        <h3>Panel klienta</h3>
                        <a href="/site/login">Zaloguj</a>
                        <a href="/client/register">Zalóż konto</a>
                        <a href="/client/profile">Twoje konto</a>
                        <a href="/shop/cart">Koszyk</a>
                    </div>
                    <div class="col-lg-6 facebook text-center">
                        <div class="fb-page" data-href="https://www.facebook.com/facebook" data-width="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id))
                                    return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.7&appId=1434596460171508";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-muted">
                    <?php if ($_SERVER['REQUEST_URI'] == '/') { ?>
                        <?= Box::forceGet('seo_text')->content; ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="clear20"></div>
        <div class="line"></div>

        <div class="container">
            <div class="row">
                <div class="clear20"></div>
                <div class="col-md-6 text-left">
                    Copyright <?= date("Y"); ?> SCK. All right reserved
                </div>
                <div class="col-md-6 text-right">
                    Projektowanie zaawansowanych sklepów internetowych <a href="http://guido.com.pl">GUIDO</a>
                </div>
            </div>
        </div>
        <div class="clear20"></div>
        <span id="toTop" onclick="$('html, body').animate({scrollTop: 0}, 200);"></span>
        <?php $this->registerJs('
        $(window).scroll(function(){
           if( $(window).scrollTop() > 100 ) 
            $("#toTop").show();
            else
            $("#toTop").hide();
        });
    '); ?>
        <?php if (!Yii::$app->session->has("modal")): ?>
            <?php $this->registerJs("$('#newsletter-modal').modal('show'); "); ?>
            <!-- modal window -->
            <div id="newsletter-modal" class="modal fade in">
                <div class="modal-dialog">
                    <!-- content-->
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" style="padding-right: 5px;">&times;</button>

                        <div class="row">
                            <div class="col-md-6">
                                <img src="/img/popup_image.jpg" class="img-responsive" onclick="$('#newsletter-modal').modal('hide');"/>
                            </div>
                            <div class="col-md-6">
                                <div class="nheader">Często szukasz okazji cenowych produktów poleasingowych?</div>

                                <div class="description">
                                    Zapisując się do newslettera otrzymasz kupon rabatowy:<br>
                                    <b>30 PLN na pierwsze zakupy</b><br>
                                </div>
                                <form action="/newsletter/subscribe" method="post">
                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                                    <div class="form">
                                        Zawsze śledź okazje
                                        <div class="input-group">
                                            <input type="text" class="form-control email-input" placeholder="Adres e-mail" name="NewsletterEmail[mail]">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary email-button" type="submit">Zapisz się!</button>
                                            </span>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="NewsletterEmail[terms]">Akceptuję regulamin</label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php Yii::$app->session->set("modal", true); ?>
        <?php endif; ?>

        <?php $this->registerJs("$(document).delegate('*[data-toggle=\"lightbox\"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
}); "); ?>

        <?=
        CookieMonster::widget([
            'content' => [
                'buttonMessage' => 'OK', // instead of default 'I understand'
                'mainMessage' => 'Strona korzysta z plików cookies w celu realizacji usług i zgodnie z Polityką Plików Cookies. Możesz określić warunki przechowywania lub dostępu do plików cookies w Twojej przeglądarce.'
            ],
            'mode' => 'bottom'
        ])
        ?>

        <?php $this->endBody() ?>
        <!-- Kod tagu remarketingowego Google -->

        <!--------------------------------------------------

    Tagi remarketingowe nie mogą być wiązane z informacjami umożliwiającymi identyfikację

    osób ani umieszczane na stronach o tematyce należącej do kategorii kontrowersyjnych.

    Więcej informacji oraz instrukcje konfiguracji tagu znajdziesz tutaj:

    http://google.com/ads/remarketingsetup

    --------------------------------------------------->

        <script type="text/javascript">

            /* <![CDATA[ */

            var google_conversion_id = 882229760;

            var google_custom_params = window.google_tag_params;

            var google_remarketing_only = true;

            /* ]]> */

        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/882229760/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>
    </body>
</html>
<?php
$this->endPage();
