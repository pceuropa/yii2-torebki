function PoliczRate(koszyk) {
    window.open('https://wniosek.eraty.pl/symulator/oblicz/numerSklepu/1157610/wariantSklepu/1/typProduktu/0/wartoscTowarow/' + koszyk, 'Policz_rate', 'width=630,height=500,directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no');
}

$(function () {


    $("body").on("click", "#sidebar-wrapper.sidebar-filters .system-filter .filter-name", function () {
        var $this = $(this);
        $this.next(".filter-content").slideToggle();
    });


    $('body').on('click', '.remove-filters a', function (e) {
        e.preventDefault();
        $("#filter-form").trigger('reset');
        $.pjax.reload({container: '#pjax-product-list', replace: false, push: false, data: $('#filter-form').serialize()});
        if ($('#filter-form').serializeArray().length > 3) {
            $('.remove-filters').html('<a href="#">Usuń filtry</a>');
        } else {
            $('.remove-filters').html('');
        }
    });

    $('body').on('click', '#filter-form input', function () {

        $.pjax.reload({container: '#pjax-product-list', replace: false, push: false, data: $('#filter-form').serialize()});
        if ($('#filter-form').serializeArray().length > 3) {
            $('.remove-filters').html('<a href="#">Usuń filtry</a>');
        } else {
            $('.remove-filters').html('');
        }
        //$("html, body").animate({scrollTop: 0}, "fast");
    });

    $(document).on('pjax:send', function () {
        $.blockUI({
            message: '<img src="/images/ajax-loading.gif" width="150">',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                backgroundColor: 'transparent',
                color: '#000',
                border: 'none'
            }});
    });
    $(document).on('pjax:complete', function () {
        $.unblockUI();
    });

    $("#client-deliverycopyfromclient").click(function (e) {
        var $this = $(this);
        if ($this.prop('checked')) {
            $("#client-d_name").val($("#client-c_name").val());
            $("#client-d_address").val($("#client-c_address").val());
            $("#client-d_postcode").val($("#client-c_postcode").val());
            $("#client-d_city").val($("#client-c_city").val());
            $("#client-d_country").val($("#client-c_country").val());
            $("#client-d_phone").val($("#client-c_phone").val());
            $("#client-d_email").val($("#client-c_email").val());
        } else {
            $("#client-d_name").val("");
            $("#client-d_address").val("");
            $("#client-d_postcode").val("");
            $("#client-d_city").val("");
            $("#client-d_country").val("");
            $("#client-d_phone").val("");
            $("#client-d_email").val("");
        }


    });

    $("#client-fvcopyfromclient").click(function (e) {
        var $this = $(this);
        if ($this.prop('checked')) {

            $("#client-fv_address").val($("#client-c_address").val());
            $("#client-fv_postcode").val($("#client-c_postcode").val());
            $("#client-fv_city").val($("#client-c_city").val());
            $("#client-fv_country").val($("#client-c_country").val());
        } else {

            $("#client-fv_address").val("");
            $("#client-fv_postcode").val("");
            $("#client-fv_city").val("");
            $("#client-fv_country").val("");
        }
    });

    $("#client-fvcopyfromdelivery").click(function (e) {
        var $this = $(this);
        if ($this.prop('checked')) {
            $("#client-fv_company").val($("#client-d_company").val());
            $("#client-fv_address").val($("#client-d_address").val());
            $("#client-fv_postcode").val($("#client-d_postcode").val());
            $("#client-fv_city").val($("#client-d_city").val());
            $("#client-fv_country").val($("#client-d_country").val());
        } else {
            $("#client-fv_company").val("");
            $("#client-fv_address").val("");
            $("#client-fv_postcode").val("");
            $("#client-fv_city").val("");
            $("#client-fv_country").val("");
        }

    });

});