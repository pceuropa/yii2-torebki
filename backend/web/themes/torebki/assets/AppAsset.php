<?php

namespace frontend\themes\torebki\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public function init() {
        $this->sourcePath = Yii::$app->view->theme->basePath . '/assets';
        parent::init();
    }
    
//    public $basePath = '@webroot';
//    public $baseUrl = '@web';
//    public $css = [
//        'css/site.css',
//        'css/yamm.css',
//        'css/ekko-lightbox.min.css',
//    ];
//    public $js = [
//        'js/bootstrap.min.js',
//        'js/ekko-lightbox.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js',
//        '/js/app.js',
//    ];
//    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//    ];
    
    

    public $css = [
               
        'http://fonts.googleapis.com/css?family=Raleway:300,400,500,700&amp;subset=latin-ext',
        'http://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css',
        'css/yamm.css',
        'css/ekko-lightbox.min.css',
        'css/main.less',
    ];
    public $js = [               
        'js/ekko-lightbox.min.js',
        'http://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js',
        '/js/app.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        //'raoul2000\bootswatch\BootswatchAsset',
        'yii\bootstrap\BootstrapAsset',
        //'raoul2000\bootswatch\BootswatchAsset',
        //'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];

}
