<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'auth_key') ?>

    <?= $form->field($model, 'password_hash') ?>

    <?= $form->field($model, 'password_reset_token') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'default_materials') ?>

    <?php // echo $form->field($model, 'discount') ?>

    <?php // echo $form->field($model, 'modified_metal_price') ?>

    <?php // echo $form->field($model, 'default_materials_0') ?>

    <?php // echo $form->field($model, 'default_materials_1') ?>

    <?php // echo $form->field($model, 'default_materials_2') ?>

    <?php // echo $form->field($model, 'default_materials_3') ?>

    <?php // echo $form->field($model, 'metal_margin') ?>

    <?php // echo $form->field($model, 'stones_margin') ?>

    <?php // echo $form->field($model, 'labor_margin') ?>

    <?php // echo $form->field($model, 'customer_ref') ?>

    <?php // echo $form->field($model, 'has_own_margins') ?>

    <?php // echo $form->field($model, 'verified') ?>

    <?php // echo $form->field($model, 'group_id') ?>

    <?php // echo $form->field($model, 'access_level') ?>

    <?php // echo $form->field($model, 'is_deleted') ?>

    <?php // echo $form->field($model, 'last_login') ?>

    <?php // echo $form->field($model, 'first_name') ?>

    <?php // echo $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'company') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'nickname') ?>

    <?php // echo $form->field($model, 'skype') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'postcode') ?>

    <?php // echo $form->field($model, 'www') ?>

    <?php // echo $form->field($model, 'about') ?>

    <?php // echo $form->field($model, 'newsletter_agreement') ?>

    <?php // echo $form->field($model, 'state') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
