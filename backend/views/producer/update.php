<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Producer */

$this->title = Yii::t('app', 'Edytuj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Producenci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edytuj');
?>
<div class="producer-update box box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
