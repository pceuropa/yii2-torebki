<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Producer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producer-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?=  $form->field($model, 'producer_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' =>  'btn btn-success' ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
