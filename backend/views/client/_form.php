<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Użytkownik
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'c_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'c_email')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'c_phone')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'rabat')->textInput() ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Dane do fvat
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'fv')->checkbox() ?>
                <?= $form->field($model, 'fv_nip')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'fv_company')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'fv_address')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'fv_postcode')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'fv_city')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'fv_country')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dane do wysyłki
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'd_company')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_address')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_postcode')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_city')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_country')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_phone')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj') : Yii::t('app', 'Zapisz'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
