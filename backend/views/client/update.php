<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title =  $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Klienci'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->__toString(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edytuj');
?>
<div class="client-update box box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
