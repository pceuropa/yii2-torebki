<?php

use yii\helpers\Html;
use yii\widgets\ListView;


/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = Yii::t('app', 'Edytuj profil');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="container">
        <div class="text-right">
            <a href="/client/profile" class="btn btn-success">Edytuj profil</a>
            <a href="/site/logout" data-method="post" class="btn btn-success">Wyloguj</a>
        </div>
        <h2 class="beauty"><span>Edytuj profil</span></h2>
        <div class="clear20"></div>

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'view'],
            'itemView' => '_order',
            'layout' => "{items}<div class='clearfix'></div>{pager}",
        ]) ?>
        <div class="clear20"></div>
    </div>





