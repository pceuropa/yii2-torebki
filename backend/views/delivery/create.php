<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Delivery */

$this->title = Yii::t('app', 'Dodaj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dostawy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
