<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;
use frontend\models\Basket;

/* @var $this yii\web\View */
/* @var $model common\models\Delivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-2"><?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-2"><div class="clear30"></div><?= $form->field($model, 'pobranie')->checkbox() ?></div>
        <div class="col-md-4"><?= $form->field($model, 'free')->textInput(['maxlength' => true]) ?></div>
    </div>


    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_method')->checkBoxList(Basket::$payments, ['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Edytuj', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
