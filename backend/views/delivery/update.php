<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Delivery */

$this->title = 'Edytuj';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dostawy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edytuj');
?>
<div class="delivery-update box box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
