<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Statusy');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-index box box-primary">


    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'toString',
            'template:ntext',
            'mail:boolean',
            'sms:boolean',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'headerOptions' => ['width' => 90],
            ],
        ],
    ]); ?>
    </div>
</div>
