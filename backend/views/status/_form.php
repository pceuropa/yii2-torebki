<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;
use dosamigos\tinymce\TinyMceAsset;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Status */
/* @var $form yii\widgets\ActiveForm */


$this->registerJs(
    <<<'JS'
    tinymce_settings = {
    selector: 'page-content',
    height: 500,
    width:'100%',
    statusbar: true,
    convert_urls: false,
    valid_children: '+body[style]',
    plugins: [
        "advlist autolink lists link charmap print preview anchor", 
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste image"
    ],
    toolbar: 'undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code',
  };

   toggle_value=0;
    function editorToggle()
    {
        if(toggle_value) {
            var editor = tinymce.EditorManager.get('status-template').remove();
            toggle_value =0;
        } else {
            var editor = tinymce.EditorManager.createEditor('status-template',tinymce_settings).render();
            toggle_value =1;
        }
    }
JS
);
?>
<?php TinyMceAsset::register($this); ?>
<div class="status-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'template')->textarea(['rows' => 6]) ?>
    <label class="control-label">WYSWIG Editor</label>
    <?= SwitchInput::widget(
        [
            'name' => 'editor_switch',
            'value' => false,
            'id' => 'editor_switch',
            'pluginEvents' => ["switchChange.bootstrapSwitch" => "function() { editorToggle(); }"],
        ]
    ); ?>

    <?= $form->field($model, 'mail')->checkbox() ?>

    <?= $form->field($model, 'sms')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Edytuj', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
