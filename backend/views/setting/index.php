<?php

use common\models\Setting;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ustawienia');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index box box-primary">

    <?php if (YII_ENV_DEV): ?>
        <div class="box-header">
            <?= Html::a(Yii::t('app', 'Create Setting'), ['create'], ['class' => 'btn btn-sm btn-success pull-right']) ?>
        </div>
    <?php endif; ?>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'category',
                    'filter' => Setting::getFilterCategory(),
                ],
                'name',
                'value',
                'description:ntext',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'headerOptions' => ['width' => 90],
                ],
            ],
        ]); ?>
    </div>
</div>
