<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */

$this->title = Yii::t('app', 'Edytuj: ', [
    'modelClass' => 'Setting',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ustawienia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edytuj');
?>
<div class="setting-update box box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
