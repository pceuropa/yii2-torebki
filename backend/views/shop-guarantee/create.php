<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShopGuarantee */

$this->title = Yii::t('app', 'Dodaj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gwarancje'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-guarantee-create box box-success">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
