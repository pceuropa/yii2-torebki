<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShopGuarantee */

$this->title = Yii::t('app', 'Edytuj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gwarancja'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edytuj');
?>
<div class="shop-guarantee-update box box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
