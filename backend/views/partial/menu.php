<?php
use yii\widgets\Menu;
use yii\bootstrap\Html;

if (!Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => Html::icon('home') . ' ' . Yii::t('app', 'GŁÓWNE OPCJE'), 'template' => '{label}', 'linkTemplate' => '', 'options' => ['class' => 'header']];
    $menuItems[] = ['label' => Html::icon('dashboard') . ' Kokpit', 'url' => ['/site/index']];
    $menuItems[] = ['label' => Html::icon('user') . ' Klienci', 'url' => ['/client/index']];
    $menuItems[] = ['label' => Html::icon('shopping-cart') . ' Zamówienia', 'url' => ['/order/index']];
    
	if(Yii::$app->user->identity->group_id == 0){
	$menuItems[] = ['label' => Html::icon('book') . ' Produkty', 'url' => ['/product/index']];

	
		$menuItems[] = ['label' => Html::icon('cog') . ' Konfiguracja', 'template' => '{label}', 'linkTemplate' => '', 'options' => ['class' => 'header']];
		$menuItems[] = [
			'options' => ['class' => 'treeview'],
			'submenuTemplate' => "\n<ul class='treeview-menu' style='display:block'>\n{items}\n</ul>\n",
			'items' => [
				['label' => 'Vouchery', 'url' => ['/voucher/index']],
				['label' => 'Konfigurator', 'url' => ['/shop-configurator/index']],
				['label' => 'Kategorie produktów', 'url' => ['/category/index']],
				['label' => 'Gwarancje', 'url' => ['/shop-guarantee/index']],
				['label' => 'Producenci', 'url' => ['/producer/index']],
				['label' => 'Statusy', 'url' => ['/status/index']],
				['label' => 'Dostawa', 'url' => ['/delivery/index']],
				['label' => 'Ustawienia', 'url' => ['/setting/index']],
				['label' => 'Użytkownicy', 'url' => ['/user/index']],
			],
		];


		$menuItems[] = ['label' => Html::icon('cog') . ' CMS', 'template' => '{label}', 'linkTemplate' => '', 'options' => ['class' => 'header']];
		$menuItems[] = [
			'options' => ['class' => 'treeview'],
			'submenuTemplate' => "\n<ul class='treeview-menu' style='display:block'>\n{items}\n</ul>\n",
			'items' => [
				['label' => 'Strony', 'url' => ['/cms/page/index']],
				['label' => 'Banery', 'url' => ['/cms/banner/index']],
				//      ['label' => 'I18n', 'url' => ['/cms/i18n/index']],
				['label' => 'Boksy', 'url' => ['/cms/box/index']],
			],
		];
	}

    $menuItems[] = [
        'label' => '<i class="fa fa-sign-out"></i> <span>' . Yii::t('app', 'Wyloguj') . '</span>',
        'url' => ['/site/logout'],
        'template' => '<a href="{url}" data-method="post">{label}</a>',
        'itemOptions' => ['class' => 'text-danger']
    ];

    echo Menu::widget([
        'encodeLabels' => false,
        'options' => ['class' => 'sidebar-menu'],
        'items' => $menuItems,
    ]);
}