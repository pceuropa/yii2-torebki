<?php

/* @var $this yii\web\View */

use backend\widgets\LastOrder;
use backend\widgets\SalesGraph;

$this->title = 'Dashboard';
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-6">
            <?= LastOrder::widget(); ?>
        </div>
        <div class="col-md-6">
            <?= SalesGraph::widget(); ?>
        </div>
    </div>
</div>
