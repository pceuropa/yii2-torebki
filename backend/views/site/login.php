<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="login-box">
    <div class="login-logo">
        <?= Html::encode($this->title) ?>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <p class="login-box-msg"><?= Yii::t('app', 'Sign in to start your session'); ?></p>
        <?= $form->field($model, 'username', ['template' => "<div class=\"form-group has-feedback\">{input}<span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span></div>\n<div>{error}</div>"])->textInput(['placeholder' => 'Email'])->label(false) ?>
        <?= $form->field($model, 'password', ['template' => "<div class=\"form-group has-feedback\">{input}<span class=\"glyphicon glyphicon-lock form-control-feedback\"></span></span></div>\n<div>{error}</div>"])->passwordInput(['placeholder' => 'Hasło'])->label(false) ?>

        <div class="row">
            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-xs-8\"><label>{input} {label}</label></div>",
            ]) ?>
            <div class="col-xs-4">
                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

        <a href="/users/remind"><?= Yii::t('app', 'Forgot password'); ?></a><br>
    </div>
    <!-- /.login-box-body -->
</div>
