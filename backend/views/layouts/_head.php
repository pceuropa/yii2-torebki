<?php 
use yii\helpers\Html;
 ?>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?= Html::csrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>

<meta name="google-site-verification" content="z_IjrKFw3PGvsWQsAP6MLDNpqkH0-mBkKx3OCsW6gmY"/>
<meta name="google-site-verification" content="t2pFXmSimOABNTUO6N_J-44uXHjRgwcsJ9y9E9Rdfn0" />

<?php $this->head() ?>
<script>
  (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
              m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-77755729-1', 'auto');
  ga('send', 'pageview');
</script>
