<?php
use yii\web\JqueryAsset;
use app\modules\cms\models\Banner;
use app\models\ProductSearch;

$this->registerCssFile('@web/css/owl.carousel.min.css');
$this->registerCssFile('@web/css/owl.theme.default.min.css');
$this->registerJsFile('@web/js/owl.carousel.min.js', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/js/app_start.js', ['depends' => [JqueryAsset::className()]]);
?>

<div class="container">
  <div class="row">
    <div class="col-md-8 col-sm-12">
    <div class="owl-carousel owl-carousel-big">
        <?php foreach (Banner::getList() as $banner): ?>
            <div class="banner-container">
                <a href="<?= $banner->link; ?>">
                    <div class='banner-content'><?= $banner->content; ?></div>
                    <img src="/banner/<?= $banner['photo']; ?>" class="img-responsive"/>
                    <button class="btn btn-success btn-orange">Sprawdź</button>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    </div>

    <div class="col-md-4 hidden-sm hidden-xs">
    <div class="owl-carousel-mini-container">
        <h3><?= Yii::t('app', 'Najcześciej kupowane'); ?></h3>
        <div class="owl-carousel owl-carousel-mini">
            <?php foreach (ProductSearch::byPromotion(5) as $row): ?>
                <div>                                                   
                    <?= $this->render('//product/_slider', ['model' => $row]); ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    </div>
  </div>
</div>
