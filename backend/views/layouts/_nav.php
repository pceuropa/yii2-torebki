<?php 
use yii\helpers\Html;
use app\models\Category;
use app\models\Basket;
use yii\helpers\Url;
?>


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <a href="/"><?= Html::img('/images/logo.png', ['class' => 'img-responsive logo']); ?></a>
        </div>
        <div class="col-md-5">
            <form id="searchBar" class="search-bar-wrapper" method="get" action="<?= Url::to(['product/search']); ?>">
                <div class="search-bar">
                    <div class="search-text">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                        <input type="text" autocomplete="off" value="" name="ProductSearch[name]" placeholder="Szukana fraza">
                    </div>
                    <div class="search-groups visible-lg visible-md">
                        <select name="ProductSearch[category_id]">
                            <option value="">wszystkie</option>
                            <?php foreach (Category::getMain() as $category): ?>
                                <option value="<?= $category->id; ?>"><?= $category->title; ?></option>
                                <?php $childs = $category->getChildLimit(21)->all(); ?>
                                <?php foreach ($childs as $child): ?>
                                    <option value="<?= $child->id; ?>"> -- <?= $child->title; ?></option>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="search-button">
                        <button class="btn btn-success btn-orange" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </form>

        </div>
        <div class="col-md-4">
            <div class="bars">
                <div class="phone menu-bars text-center">
                    <a href="<?= Url::to(['site/contact']); ?>">
                        <i class="glyphicon glyphicon-envelope text-gray" style="color: #B2B2B2; font-size: 24pt;" aria-hidden="true"></i>
                        <br />
                        <span style="color: #2D2D2D;">
                            <?= Yii::t('app', 'Napisz do nas') ?>
                        </span>
                    </a>
                </div>
                <div class="user text-center menu-bars">
                    <a href="<?= Url::to(['client/profile']); ?>">
                        <?= Html::img('@web/images/user.png', ['class' => 'img-responsive']); ?>
                        <span>Twój profil</span>
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <?= Yii::$app->user->identity->toString ?>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="cart text-center menu-bars">
                    <a href="<?= Url::to(['shop/card']); ?>" id="basket-ico">
                        <?= Html::img('@web/images/cart.png', ['class' => 'img-responsive']); ?>
                        <?php if ($count = Basket::scount()): ?>
                            <span class="badge badge-danger"><?= $count; ?></span>
                        <?php endif; ?>
                        <span>Koszyk</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
