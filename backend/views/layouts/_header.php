<?php 
use app\models\Product;
use app\models\Category;
use app\models\Basket;
use app\widgets\Alert;
use app\models\ProductSearch;
use yii\helpers\Html;
use app\modules\cms\models\Box;
 ?>

<div class="text-center top-address hidden"> <?= Box::forceGet('top-address')->content; ?></div>
<nav>
    <?= $this->render('_nav'); ?>
</nav>

<div class="container">
    <?= Alert::widget() ?>
</div>

<div class="header <?= !$isHome ? 'header-small' : ''; ?>">
<div class="navbar yamm">
  <div class="container">
      <div class="navbar-header visible-xs visible-sm">
          <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
      </div>
      <div id="navbar-collapse-1" class="navbar-collapse collapse padding-0">
          <ul class = "nav navbar-nav">
              <?php
              foreach (Category::getMain() as $category):
                  $active = '';
                  if (isset($_GET['id']) && !empty($_GET['id'])) {
                      $productModel = Product::find()->where(['id' => (int) $_GET['id']])->one();
                      if ($productModel) {
                          $categoryModel = \app\models\Category::find()->where(['id' => $productModel->category_id])->one();
                      } else {
                          $categoryModel = \app\models\Category::find()->where(['id' => (int) $_GET['id']])->one();
                      }
                      if ($category->slug == $categoryModel->parentcategory->slug || $category->slug == $categoryModel->slug) {
                          $active = 'color: #f14837;border-top: 2px solid #f14837;';
                      }
                  }
                  ?>
                  <li>
                      <a href="<?= $category->getLink(); ?>"  style="<?= $active ?>" aria-expanded="false">
                          <?php echo Html::img($baseUrl . '/images/category_' . $category->slug . '.png', ['class' => '']); ?>
                          <?= $category->title; ?>
                      </a>
                  </li>
                  <?php

              endforeach;
              ?>
          </ul>
      </div>
  </div>
</div>
<?php if ($isHome): ?>
  <?php
  $this->registerCssFile('@web/css/owl.carousel.min.css');
  $this->registerCssFile('@web/css/owl.theme.default.min.css');
  $this->registerJsFile('@web/js/owl.carousel.min.js', ['depends' => [JqueryAsset::className()]]);
  $this->registerJsFile('@web/js/app_start.js', ['depends' => [JqueryAsset::className()]]);
  ?>

  <div class="container">
      <div class="row">
          <div class="col-md-8 col-sm-12">
              <div class="owl-carousel owl-carousel-big">
                  <?php foreach (Banner::getList() as $banner): ?>
                      <div class="banner-container">
                          <a href="<?= $banner->link; ?>">
                              <div class='banner-content'><?= $banner->content; ?></div>
                              <img src="/banner/<?= $banner['photo']; ?>" class="img-responsive"/>
                              <button class="btn btn-success btn-orange">Sprawdź</button>
                          </a>
                      </div>
                  <?php endforeach; ?>
              </div>
          </div>
          <div class="col-md-4 hidden-sm hidden-xs">
              <div class="owl-carousel-mini-container">
                  <h3><?php echo Yii::t('app', 'Najcześciej kupowane'); ?></h3>
                  <div class="owl-carousel owl-carousel-mini">


                      <?php foreach (ProductSearch::byPromotion(5) as $row): ?>
                          <div>                                                   
                              <?= $this->render('//product/_slider', ['model' => $row]); ?>
                          </div>
                      <?php endforeach; ?>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <?php echo $this->render('_four_boxes', ['baseUrl' => $baseUrl]); ?>
  <div class="clearfix"></div>
<?php endif; ?>
</div>
