<?php

use common\models\ShopConfigurator;
use common\models\ShopGuarantee;
use dosamigos\fileupload\FileUploadUI;
use backend\modules\cms\models\I18n;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;
use dosamigos\tinymce\TinyMceAsset;
use kartik\widgets\SwitchInput;



$this->registerJs(
    <<<'JS'
    tinymce_settings = {
    selector: 'page-content',
    height: 500,
    width:'100%',
    statusbar: true,
    convert_urls: false,
    valid_children: '+body[style]',
    plugins: [
        "advlist autolink lists link charmap print preview anchor", 
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste image"
    ],
    toolbar: 'undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code',
  };

   toggle_value=0;
    function editorToggle()
    {
        if(toggle_value) {
            var editor = tinymce.EditorManager.get('product-description').remove();
            toggle_value =0;
        } else {
            var editor = tinymce.EditorManager.createEditor('product-description',tinymce_settings).render();
            toggle_value =1;
        }
    }
JS
);
/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<?php TinyMceAsset::register($this); ?>
<div class="product-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'part_number')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-4">


            <?= $form->field($model, 'category_name')->widget(AutoComplete::classname(), [
                'options' => [
                    'class' => 'form-control',
                    'value' => $model->category ? $model->category->__toString() : '',
                ],
                'clientOptions' => [
                    'source' => new JsExpression("function(request, response) {
                                    $.getJSON('/category/autocomplete', {
                                        term: request.term
                                    }, response);
                                }"),
                    'select' => new JsExpression("function( event, ui ) {
                                    $('#product-category_id').val(ui.item.id);
                                }"),
                ],
                'class' => 'form-control',
            ]) . $form->field($model, 'category_id')->hiddenInput()->label(false) ?>
        </div>
    </div>


    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <label class="control-label">WYSWIG Editor</label>
    <?= SwitchInput::widget(
        [
            'name' => 'editor_switch',
            'value' => false,
            'id' => 'editor_switch',
            'pluginEvents' => ["switchChange.bootstrapSwitch" => "function() { editorToggle(); }"],
        ]
    ); ?>


    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'price_import')->textInput() ?></div>
        <div class="col-md-4"> <?= $form->field($model, 'price_basic')->textInput() ?></div>
        <div class="col-md-4"><?= $form->field($model, 'price')->textInput() ?></div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'promotion')->dropDownList(I18n::yesNo()) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'recommended')->dropDownList(I18n::yesNo()) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'bestseller')->dropDownList(I18n::yesNo()) ?></div>
    </div>

    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'quantity')->textInput() ?></div>
        <div class="col-md-3"><?php //  = $form->field($model, 'producer_id')->textInput() ?></div>
        <div class="col-md-3"><?= $form->field($model, 'warranty')->dropDownList(ShopGuarantee::getFilterList()) ?></div>
    </div>



    <?= $form->field($model, 'active')->dropDownList(I18n::yesNo()) ?>
    <?= $form->field($model,'konfigurator')->checkboxList(ShopConfigurator::getFilterList()); ?>

    <?php if ($model->isNewRecord === false):?>    
    <div class="row">
        <div class="col col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Zdjęcia
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'folder')->hiddenInput() ?>
                    <?=
                    FileUploadUI::widget([
                        'model'         => $model,
                        'attribute'     => 'image',
                        'url'           => ['upload/add', 'folder' => $model->folder],
                        'gallery'       => false,
                        'fieldOptions'  => [
                            'accept' => 'image/*'
                        ],
                        'clientOptions' => [
                            'maxFileSize' => 2000000
                        ],
                        'clientEvents'  => [
                            'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <?php if ($model->images): ?>
        <div class="col col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Załadowne zdjęcia
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php foreach ($model->images as $image): ?>
                            <div class="col-md-2">
                                <img src="<?= $image->getUrl('250x250'); ?>" class="img-responsive"/>
                                <input type="radio" name="mainImage" value="<?= $image->id; ?>" <?php if($model->photo == $image->photo) echo 'checked="checked"'; ?>/> Główne
                                <input type="checkbox" name="deleteImage[<?= $image->id; ?>]"/> usuń
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

        </div>
        <?php endif; ?>
    </div>
     <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
