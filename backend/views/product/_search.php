<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'part_number') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'price_import') ?>

    <?php // echo $form->field($model, 'price_basic') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'promotion') ?>

    <?php // echo $form->field($model, 'bargain') ?>

    <?php // echo $form->field($model, 'recommended') ?>

    <?php // echo $form->field($model, 'bestseller') ?>

    <?php // echo $form->field($model, 'new') ?>

    <?php // echo $form->field($model, 'quantity') ?>

    <?php // echo $form->field($model, 'producer') ?>

    <?php // echo $form->field($model, 'warranty') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'czas_dostawy') ?>

    <?php // echo $form->field($model, 'vat') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'open') ?>

    <?php // echo $form->field($model, 'mark') ?>

    <?php // echo $form->field($model, 'konfigurator') ?>

    <?php // echo $form->field($model, 'widget_offer') ?>

    <?php // echo $form->field($model, 'widget_recommend') ?>

    <?php // echo $form->field($model, 'widget_price') ?>

    <?php // echo $form->field($model, 'update_done') ?>

    <?php // echo $form->field($model, 'update_new') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
