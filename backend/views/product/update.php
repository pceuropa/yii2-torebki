<?php use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = Yii::t('app', 'Edytuj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Produkty'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edytuj');
?>
<div class="product-update box box-warning">
    <div class="box-body">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Dane</a></li>
            <li role="presentation"><a href="#attrs" aria-controls="home" role="tab" data-toggle="tab">Atrybuty</a></li>
        </ul>
    </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <?= $this->render('_form', [ 'model' => $model, ]) ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="attrs">
            <?= $this->render('_atts', [ 'model' => $model, 'attributes' => $attributes, 'atts' => $atts ]) ?> 
        </div>

    </div>
</div>
