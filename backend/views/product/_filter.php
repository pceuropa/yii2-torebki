<form action="" method="post" id="ffilter">
    <div class="clear20"></div>
    <div class="col-xs-3">
        <labeL>Sortuj:</labeL>
    </div>
    <div class="col-xs-9">

    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
    <select class="form-control" name="sort" onchange="$('#ffilter').submit()">
      <option value="newest" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'newest') ? 'selected="selected"' : '' ?>>Od najnowszych</option>
      <option value="oldest" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'oldest') ? 'selected="selected"' : '' ?>>Od najstarszych</option>
      <option value="priceA" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'priceA') ? 'selected="selected"' : '' ?>>Od najtańszych</option>
      <option value="priceZ" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'priceZ') ? 'selected="selected"' : '' ?>>Od najdroższych</option>
      <option value="nameA" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'nameA') ? 'selected="selected"' : '' ?>>Od A-Z</option>
      <option value="nameZ" <?= (Yii::$app->session->has('sort-list') && Yii::$app->session->get('sort-list') == 'nameZ') ? 'selected="selected"' : '' ?>>Od Z-A</option>
    </select>

    </div>
    <div class="clear10"></div>
    <div class="col-xs-3">
        <labeL>Wyświetl:</labeL>
    </div>

    <div class="col-xs-9">
        <select class="form-control" name="type" onchange="$('#ffilter').submit()">
            <option value="all" <?= (Yii::$app->session->has('type-list') && Yii::$app->session->get('type-list') == 'all') ? 'selected="selected"' : '' ?>>Wszystkie</option>
            <option value="new" <?= (Yii::$app->session->has('type-list') && Yii::$app->session->get('type-list') == 'new') ? 'selected="selected"' : '' ?>>Nowości</option>
            <option value="recommend" <?= (Yii::$app->session->has('type-list') && Yii::$app->session->get('type-list') == 'recommend') ? 'selected="selected"' : '' ?>>Polecane</option>
        </select>
    </div>
</form>
