<?php
use backend\modules\cms\models\I18n;
use common\models\ShopAttribute;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="product-form box-body">

    <?php $form = ActiveForm::begin(); ?>
    <?php foreach ($attributes as $attId => $name): ?>

        <div class="col-md-6">
            <div class="row">
                <div class="col-sm-6">
        <input type="checkbox" <?php if(isset($atts[$attId])) echo 'checked="checked"'; ?> name="ShopProductAttribute[<?= $attId; ?>][attribute_id]" /> 
        <?= $name; ?>
                </div>

        <div class="col-sm-6">
        <input type="text" name="ShopProductAttribute[<?= $attId; ?>][value]" <?php if(isset($atts[$attId])) echo 'value="'.$atts[$attId]['value'].'"'; ?> placeholder="wartość" />
                    <input type="text" name="ShopProductAttribute[<?= $attId; ?>][unit]"  <?php if(isset($atts[$attId])) echo 'value="'.$atts[$attId]['unit'].'"'; ?>  style="width:60px" placeholder="unit" />
                </div>

            </div>
        </div>
        <?php endforeach; ?>
    <div class="clear20"></div>
    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
