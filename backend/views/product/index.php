<?php
use backend\modules\cms\models\I18n;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Produkt');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index box box-primary">

    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Pobierz nowe produkty i zdjęcia'), ['/import/import'], ['class' => 'btn btn-sm btn-info pull-right']) ?>
        <?= Html::a(Yii::t('app', 'Dodaj'), ['create'], ['class' => 'btn btn-sm btn-success pull-right']) ?>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute' => 'category_id',
                    'value' => 'category.title',
                ],
                [
                    'attribute' => 'name',
                    'value' => 'name',
                ],
                // 'price_import',
                // 'price_basic',
                // 'price',
                // 'promotion',
                // 'bargain',
                // 'recommended',
                // 'bestseller',
                // 'new',
                'quantity',
                // 'producer',
                // 'warranty',
                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return $model->product_id ? 'Nie' : 'Tak';
                    },
                    'filter' => I18n::yesNo(),
                ],
                // 'czas_dostawy',
                // 'vat',
                // 'photo',
                [
                    'attribute' => 'active',
                    'format' => 'boolean',
                    'filter' => I18n::yesNo()
                ],
                // 'open',
                // 'mark',
                // 'konfigurator:ntext',
                // 'widget_offer',
                // 'widget_recommend:boolean',
                // 'widget_price:boolean',
                // 'update_done',
                // 'update_new',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {copy} {delete} {promote}',
                    'headerOptions' => ['width' => 100],
                    'buttons' => [
                        'atts' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-th-list"></span>', $url, [
                                        'title' => Yii::t('yii', 'Atrybuty'),
                            ]);
                        },
                                'copy' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, [
                                        'title' => Yii::t('yii', 'Kopiuj'),
                                        'data' => ['confirm' => Yii::t('yii', 'Napewno skopiować produkt?')]
                            ]);
                        },
                        'promote' => function ($url, $model) {
                            $type=1;
                            $title =  Yii::t('yii', 'Promuj');
                            if($model->promotion==1){
                                $type=0;
                                $title =  Yii::t('yii', 'Usuń promowanie');
                            }
                            return Html::a('<span class="glyphicon glyphicon-usd"></span>', ['product/promote', 'id'=>$model->id, 'type'=>$type], [
                                        'title' =>$title,
                            ]);
                        },
                            ]
                        ],
                    ],
                ]);
                ?>
    </div>
</div>
