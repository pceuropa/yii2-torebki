<?php

use common\models\ShopConfigurator;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ShopConfiguratorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Konfigurator');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-configurator-index box box-primary">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Dodaj'), ['create'], ['class' => 'btn btn-sm btn-success pull-right']) ?>
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'type',
                'value'=>'type',
                'filter'=> ShopConfigurator::getTypeFilterList(),
            ],
            'value',
            'price',
			'product_id',
           /* [
                'attribute' => 'product_id',
                'value' => 'product.name',
            ],*/

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'headerOptions' => ['width' => 90],
            ],
        ],
    ]); ?>
    </div>
</div>
