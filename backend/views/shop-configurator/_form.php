<?php

use common\models\ShopConfigurator;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\ShopConfigurator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-configurator-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList(ShopConfigurator::getTypeFilterList()) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'product_name')->widget(AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control',
            'value' => $model->product ? $model->product->__toString() : '',
        ],
        'clientOptions' => [
            'source' => new JsExpression("function(request, response) {
                                    $.getJSON('/product/autocomplete', {
                                        term: request.term
                                    }, response);
                                }"),
            'select' => new JsExpression("function( event, ui ) {
                                    $('#shop-configurator-product_id').val(ui.item.id);
                                }"),
        ],
        'class' => 'form-control',
    ]) . $form->field($model, 'product_id')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
