<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShopConfigurator */

$this->title = Yii::t('app', 'Edytuj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Konfigurator'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edytuj');
?>
<div class="shop-configurator-update box box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
