<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form box-body">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dane zamówienia
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'date')->textInput() ?>
                <?= $form->field($model, 'status')->textInput() ?>
                <?= $form->field($model, 'products_price')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'transport_price')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'transport_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'transport_number')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'payment_price')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'payment_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'payment_number')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'notice')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Rabat
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'rabat_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'rabat')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'voucher_price')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'voucher_percent')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'voucher_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

    </div>
    <div class="col-md-6">


        <div class="panel panel-default">
            <div class="panel-heading">
                Użytkownik
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'client_name')->widget(AutoComplete::classname(), [
                    'options'       => [
                        'class' => 'form-control',
                        'value' => $model->client ? $model->client->__toString() : '',
                    ],
                    'clientOptions' => [
                        'source' => new JsExpression("function(request, response) {
                                    $.getJSON('/client/autocomplete', {
                                        term: request.term
                                    }, response);
                                }"),
                        'select' => new JsExpression("function( event, ui ) {
                                    $('#order-client_id').val(ui.item.id);
                                }"),
                    ],
                    'class'         => 'form-control',
                ])
                .
                $form->field($model, 'client_id')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'c_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'c_email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'c_phone')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Dane do dostawy
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'd_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_company')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_address')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_postcode')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_city')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_country')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'd_phone')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Dane do faktury
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'fv')->checkbox() ?>

                <?= $form->field($model, 'fv_nip')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fv_company')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fv_address')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fv_postcode')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fv_city')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'fv_country')->textInput(['maxlength' => true]) ?>

            </div>
        </div>
    </div>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
