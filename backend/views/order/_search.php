<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'products_price') ?>

    <?= $form->field($model, 'transport_price') ?>

    <?php // echo $form->field($model, 'transport_name') ?>

    <?php // echo $form->field($model, 'transport_number') ?>

    <?php // echo $form->field($model, 'payment_price') ?>

    <?php // echo $form->field($model, 'payment_name') ?>

    <?php // echo $form->field($model, 'payment_number') ?>

    <?php // echo $form->field($model, 'notice') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'fv') ?>

    <?php // echo $form->field($model, 'fv_nip') ?>

    <?php // echo $form->field($model, 'fv_company') ?>

    <?php // echo $form->field($model, 'fv_address') ?>

    <?php // echo $form->field($model, 'fv_postcode') ?>

    <?php // echo $form->field($model, 'fv_city') ?>

    <?php // echo $form->field($model, 'fv_country') ?>

    <?php // echo $form->field($model, 'd_name') ?>

    <?php // echo $form->field($model, 'd_company') ?>

    <?php // echo $form->field($model, 'd_address') ?>

    <?php // echo $form->field($model, 'd_postcode') ?>

    <?php // echo $form->field($model, 'd_city') ?>

    <?php // echo $form->field($model, 'd_country') ?>

    <?php // echo $form->field($model, 'd_phone') ?>

    <?php // echo $form->field($model, 'c_name') ?>

    <?php // echo $form->field($model, 'c_surname') ?>

    <?php // echo $form->field($model, 'c_email') ?>

    <?php // echo $form->field($model, 'c_phone') ?>

    <?php // echo $form->field($model, 'rabat_name') ?>

    <?php // echo $form->field($model, 'rabat') ?>

    <?php // echo $form->field($model, 'voucher_price') ?>

    <?php // echo $form->field($model, 'voucher_percent') ?>

    <?php // echo $form->field($model, 'voucher_name') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
