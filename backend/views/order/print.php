<?php

use common\models\Status;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = '#' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zamówienia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
<div class="col-xs-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            Dane zamówienia
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'date',
                    'statusName',
                    'products_price',
                    'transport_price',
                    'transport_name',
                    'payment_price',
                    'payment_name',
                    'payment_number',
                    'notice',
                ],
            ]) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            Użytkownik
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'c_name',
                    'c_email:email',
                    'c_phone',
                ],
            ]) ?>
        </div>
    </div>
</div>

<div class="col-xs-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            Dane do dostawy
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'd_company',
                    'd_name',
                    'd_address',
                    'd_postcode',
                    'd_city',
                    'd_country',
                    'd_phone',
                ],
            ]) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            Dane do faktury
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'fv:boolean',
                    'fv_nip',
                    'fv_company',
                    'fv_address',
                    'fv_postcode',
                    'fv_city',
                    'fv_country',
                ],
            ]) ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Produkty
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => new ArrayDataProvider([
                            'allModels' => $model->items,
                        ]),
                        'filterModel' => false,
                        'columns' => [
                            'name',
                            'price_one',
                            'quantity',
                            'options',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
</div>