<?php

use common\models\Category;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'category_name')->widget(AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control',
            'value' => $model->parentCategory ? $model->parentCategory->__toString() : '',
        ],
        'clientOptions' => [
            'source' => new JsExpression("function(request, response) {
                                    $.getJSON('/category/autocomplete', {
                                        term: request.term
                                    }, response);
                                }"),
            'select' => new JsExpression("function( event, ui ) {
                                    $('#category-parent').val(ui.item.id);
                                }"),
        ],
        'class' => 'form-control',
    ]) . $form->field($model, 'parent')->hiddenInput()->label(false)
    ?>

    <?php echo $form->field($model, 'import_id')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'active')->checkbox() ?>
    <?= $form->field($model, 'margin')->textInput() ?>

    <?php if (!$model->isNewRecord): ?>
        <?php $attributes = Category::getProductFilters($model->id); ?>
    <?= $form->field($model, 'filters')->checkboxList($attributes); ?>
<?php endif; ?>


    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
