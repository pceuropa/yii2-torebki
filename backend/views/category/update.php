<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title =  $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kategorie'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="category-update box box-warning">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
