<?php

namespace backend\widgets;

use common\models\Order;
use yii\base\Widget;

class SalesGraph extends Widget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $ordersByMonth = Order::find()->select(['DATE_FORMAT(date,\'%Y-%m\') as date','count(id) as count'])->groupBy('date')->orderBy('date desc')->limit(20)->asArray()->all();
        $ordersByMonth = $this->getJsTableChart($ordersByMonth);
        return $this->render('sales_graph', ['ordersByMonth' => $ordersByMonth]);
    }

    private function getJsTableChart($ordersByMonth)
    {
        $output = [];
        foreach($ordersByMonth as $order)
        {
            $output[] = "{y: '{$order['date']}', item1: {$order['count']}}";
        }
        $output = implode(',',$output);
        return $output;


    }
}