<?php

namespace backend\widgets;

use common\models\Order;
use yii\base\Widget;

class LastOrder extends Widget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $orders = Order::find()->orderBy('id desc')->limit(15)->all();

        return $this->render('last_order', ['orders' => $orders]);
    }
}