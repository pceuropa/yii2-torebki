<div class="box box-solid bg-light-blue-gradient">
    <div class="box-header ui-sortable-handle">
        <h3 class="box-title">Sprzedaż</h3>
    </div>
    <div class="box-body border-radius-none">
        <div class="chart" id="line-chart" style="height: 250px;"></div>
    </div>
</div>
<?php $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>

<?php $this->registerJS("

  var line = new Morris.Line({
    element: 'line-chart',
    resize: true,
    data: [".$ordersByMonth
    ."],
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Zamówień'],
    lineColors: ['#efefef'],
    lineWidth: 2,
    hideHover: 'auto',
    gridTextColor: '#fff',
    gridStrokeWidth: 0.4,
    pointSize: 4,
    pointStrokeColors: ['#efefef'],
    gridLineColor: '#efefef',
    gridTextFamily: 'Open Sans',
    gridTextSize: 10
  }); 

"); ?>
