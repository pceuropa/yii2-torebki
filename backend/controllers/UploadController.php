<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * UploadController
 */
class UploadController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                'ruleConfig' => [
                    'class' => 'common\components\AccessRule',
                ],
                'rules'      => [
                    [
                        'actions' => ['add', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionAdd($folder)
    {
        $file      = UploadedFile::getInstanceByName('Product[image]');
        $directory = \Yii::getAlias('@backend/web/tmp') . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        if ($file) {
            $uid      = uniqid(time(), true);
            $fileName = $uid . '.' . $file->extension;
            $filePath = $directory . $fileName;
            if ($file->saveAs($filePath)) {
                $path = '/tmp/' . $folder . DIRECTORY_SEPARATOR . $fileName;

                return Json::encode([
                        'files' => [
                            [
                                'name'         => $fileName,
                                'size'         => $file->size,
                                "url"          => $path,
                                "thumbnailUrl" => (stripos(mime_content_type($filePath), 'video') === false ? $path : '/css/movie.png'),
                                "deleteUrl"    => '/upload/delete?name=' . $fileName . '&folder=' . $folder,
                                "deleteType"   => "POST",
                            ],
                        ],
                ]);
            }
        }

        return '';
    }

    public function actionDelete($name, $folder)
    {
        $directory = \Yii::getAlias('@backend/web/tmp') . DIRECTORY_SEPARATOR . $folder;
        if (is_file($directory . DIRECTORY_SEPARATOR . $name)) {
            unlink($directory . DIRECTORY_SEPARATOR . $name);
        }
        $files  = FileHelper::findFiles($directory);
        $output = [];
        foreach ($files as $file) {
            $path              = '/tmp/' . $folder . DIRECTORY_SEPARATOR . basename($file);
            $output['files'][] = [
                'name'         => basename($file),
                'size'         => filesize($file),
                "url"          => $path,
                "thumbnailUrl" => (stripos(mime_content_type($file), 'video') === false ? $path : '/css/movie.png'),
                "deleteUrl"    => '/upload/delete?name=' . basename($file) . '&folder=' . $folder,
                "deleteType"   => "POST",
            ];
        }

        return Json::encode($output);
    }

}
