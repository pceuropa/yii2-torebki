<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use common\models\User;
use common\models\ShopProductAttribute;
use common\models\ShopAttribute;
use common\models\Product;
use common\models\ProductSearch;
use backend\components\AccessRule;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Zmiany zostały zapisane'));
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $model = $this->findModel($id);
        $model->getKonfigurator();
        $model->folder = $model->getImageFolderName();

        if (isset($_POST['ShopProductAttribute'])) {
            ShopProductAttribute::deleteAll(['product_id' => $id]);

            foreach ($_POST['ShopProductAttribute'] as $aid => $atts) {

                if (!isset($atts['attribute_id']))
                    continue;

                $sa = new ShopProductAttribute();
                $sa->value = $atts['value'];
                $sa->unit = $atts['unit'];
                $sa->category_id = $model->category_id;
                $sa->product_id = $id;
                $sa->attribute_id = $aid;
                $sa->save();
            }

            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Zmiany zostały zapisane'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
        elseif ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Zmiany zostały zapisane'));
                return $this->redirect(['update', 'id' => $model->id]);
            } 
            var_dump($model->errors);
        } 

        return $this->render('update', [
            'model' => $model,
            'attributes' => ShopAttribute::getFilterList($model->category_id),
            'atts' => ShopProductAttribute::getFilterListProduct($id)

        ]);
    }

    /**
     * Copy an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCopy($id) {
        $model = $this->findModel($id);
        $model->getKonfigurator();
        $model->folder = $model->getImageFolderName();

        $new = new Product();
        $new->setAttributes($model->getAttributes(null, ['id']));
        $new->save(false);

        $attributes = $model->productAttributes;

        foreach ($attributes as $attribute) {
            $spa = new ShopProductAttribute();
            $spa->setAttributes($attribute->getAttributes(null, ['id', 'product_id']));
            $spa->product_id = $new->id;
            $spa->save(false);
        }


        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Zmiany zostały zapisane'));
        return $this->redirect(['update', 'id' => $new->id]);
    }

    public function actionAtts($id) {
        $model = $this->findModel($id);

        if (isset($_POST['ShopProductAttribute'])) {
            ShopProductAttribute::deleteAll(['product_id' => $id]);
            // var_dump($_POST); die();
            foreach ($_POST['ShopProductAttribute'] as $aid => $atts) {
                if (!isset($atts['attribute_id']))
                    continue;
                $sa = new ShopProductAttribute();
                $sa->value = $atts['value'];
                $sa->unit = $atts['unit'];
                $sa->category_id = $model->category_id;
                $sa->product_id = $id;
                $sa->attribute_id = $aid;
                $sa->save(false);
            }

            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Zmiany zostały zapisane'));
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('atts', [
                'model' => $model,
                'attributes' => ShopProductAttribute::getFilterListProduct($id)
            ]);
        }
    }

    public function actionPromote($id, $type = 1) {
        $model = $this->findModel($id);
        if ($model) {
            $model->promotion = $type;
            $model->save(false);
            $messsgae = Yii::t('app', 'Usunięto produkt z produktów promowanych');
            if ($type == 1) {
                $messsgae = Yii::t('app', 'Dodano produkt do promowanych');
            }
            Yii::$app->getSession()->setFlash('success', $messsgae);
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Nie udało się dodać produktu do promowanych!'));
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Zmiany zostały zapisane'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Autocomplete
     * @param string $term
     * @return JSON
     */
    public function actionAutocomplete($term) {
        $items = [];
        if ($list = Product::find()->andFilterWhere(['like', 'name', $term])->asArray()->all()) {
            foreach ($list as $l) {
                $items[] = ['label' => $l['name'], 'id' => $l['id'], 'value' => $l['name']];
            }
        }

        Yii::$app->response->format = 'json';
        return $items;
    }

}
