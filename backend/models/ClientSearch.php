<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Client;

/**
 * ClientSearch represents the model behind the search form about `common\models\Client`.
 */
class ClientSearch extends Client
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fv', 'rabat'], 'integer'],
            [['login', 'date', 'fv_nip', 'fv_company', 'fv_address', 'fv_postcode', 'fv_city', 'fv_country', 'd_company', 'd_name', 'd_address', 'd_postcode', 'd_city', 'd_country', 'd_phone', 'c_name', 'c_surname', 'c_email', 'c_phone', 'password', 'fav', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'fv' => $this->fv,
            'rabat' => $this->rabat,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'fv_nip', $this->fv_nip])
            ->andFilterWhere(['like', 'fv_company', $this->fv_company])
            ->andFilterWhere(['like', 'fv_address', $this->fv_address])
            ->andFilterWhere(['like', 'fv_postcode', $this->fv_postcode])
            ->andFilterWhere(['like', 'fv_city', $this->fv_city])
            ->andFilterWhere(['like', 'fv_country', $this->fv_country])
            ->andFilterWhere(['like', 'd_company', $this->d_company])
            ->andFilterWhere(['like', 'd_name', $this->d_name])
            ->andFilterWhere(['like', 'd_address', $this->d_address])
            ->andFilterWhere(['like', 'd_postcode', $this->d_postcode])
            ->andFilterWhere(['like', 'd_city', $this->d_city])
            ->andFilterWhere(['like', 'd_country', $this->d_country])
            ->andFilterWhere(['like', 'd_phone', $this->d_phone])
            ->andFilterWhere(['like', 'c_name', $this->c_name])
            ->andFilterWhere(['like', 'c_surname', $this->c_surname])
            ->andFilterWhere(['like', 'c_email', $this->c_email])
            ->andFilterWhere(['like', 'c_phone', $this->c_phone])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'fav', $this->fav])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
