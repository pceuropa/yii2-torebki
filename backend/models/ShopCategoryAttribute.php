<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shop_category_attribute".
 *
 * @property integer $category_id
 * @property integer $attribute_id
 * @property string $value
 * @property string $unit
 */
class ShopCategoryAttribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_category_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'attribute_id', 'value', 'unit'], 'required'],
            [['category_id', 'attribute_id'], 'integer'],
            [['value'], 'string', 'max' => 100],
            [['unit'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'attribute_id' => 'Attribute ID',
            'value' => 'Value',
            'unit' => 'Unit',
        ];
    }
}
