<?php

use backend\modules\cms\Cms;

$this->title = Cms::t('lbl', 'Create banner');
$this->params['breadcrumbs'][] = ['label' => Cms::t('lbl', 'Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="banners-create col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>