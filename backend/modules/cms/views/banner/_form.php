<?php

use backend\modules\cms\models\I18n;
use backend\modules\cms\Cms;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="banners-form box-body">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'language')->dropDownList(I18n::getLanguages()) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'active')->dropDownList([1 => Cms::t('lbl', 'Yes'), 0 => Cms::t('lbl', 'No')]) ?>
        </div>
    </div>

    <?= $form->field($model, 'content')->textarea(['maxlength' => true, 'rows' => 2]) ?>
    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?php if ($model->photo): ?>
        <img src="<?= Yii::$app->setting->get('frontUrl'); ?>/banner/<?= $model->photo; ?>"/>
    <?php endif; ?>

    <div class="clear20"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Cms::t('btn', 'Create') : Cms::t('btn', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
