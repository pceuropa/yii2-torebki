<?php

use backend\modules\cms\Cms;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Cms::t('lbl', 'Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <?= Html::a(Cms::t('btn', 'Create banner'), ['create'], ['class' => 'btn btn-success pull-right btn-sm']) ?>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin();
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-condensed table-striped'],
                    'columns' => [
                        'title',
                        'link',
                        'photo',
                        [
                            'attribute' => 'active',
                            'value' => function ($model) {
                                return ($model->active == 1) ? 'Yes' : 'No';
                            },
                            'filter' => [0 => 'No', 1 => 'Yes'],
                        ],
                        [
                            'attribute' => 'position',
                            'value' => function ($model) {
                                return Html::activeTextInput($model, '[' . $model->id . ']position');
                            },
                            'format' => 'raw'
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                            'headerOptions' => ['width' => 70],
                        ],
                    ],
                ]);
                echo Html::submitButton('Zapisz');
                ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</div>