<?php

use backend\modules\cms\Cms;

$this->title = Cms::t('lbl', 'Update');
$this->params['breadcrumbs'][] = ['label' => Cms::t('lbl', 'Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Cms::t('app', 'Update');
?>
<div class="row">
    <div class="boxes-update col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>