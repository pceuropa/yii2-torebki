<?php

use backend\modules\cms\Cms;
use yii\helpers\Html;
use yii\grid\GridView;


$this->title = Cms::t('lbl', 'Boxes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <?= Html::a(Cms::t('btn', 'Create box'), ['create'], ['class' => 'btn btn-success btn-sm pull-right']) ?>
            </div>
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-condensed table-striped'],
                    'columns' => [
                        'title',
                        [
                            'attribute' => 'link',
                            'content' => function ($model) {
                                return $model->getLink();
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                            'headerOptions' => ['width' => 70],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>