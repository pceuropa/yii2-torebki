<?php

use backend\modules\cms\Cms;
use yii\grid\GridView;

$this->title = Cms::t('app', 'Translations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="i18n-index box box-primary">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-condensed table-striped'],
            'columns' => [
                'language',
                'value',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'headerOptions' => ['width' => 70],
                ],
            ],
        ]); ?>
    </div>
</div>
