<?php

use backend\modules\cms\Cms;


$this->title = Cms::t('lbl', 'Translations');
$this->params['breadcrumbs'][] = ['label' => Cms::t('lbl', 'Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="i18n-create col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>