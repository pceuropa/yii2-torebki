<?php

use backend\modules\cms\Cms;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="i18n-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (YII_ENV_DEV): ?>
        <?= $form->field($model, 'var')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'lang')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <div class="row">

        <div class="col-md-8">
            <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Cms::t('btn', 'Create') : Cms::t('btn', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
