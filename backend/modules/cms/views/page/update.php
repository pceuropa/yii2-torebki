<?php

use backend\modules\cms\Cms;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Cms::t('lbl', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Cms::t('lbl', 'Update');
?>

<div class="row">
    <div class="pages-update col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>