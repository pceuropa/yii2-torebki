<?php

use backend\modules\cms\Cms;

$this->title = Cms::t('lbl', 'Create page');
$this->params['breadcrumbs'][] = ['label' => Cms::t('lbl', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="pages-create col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>