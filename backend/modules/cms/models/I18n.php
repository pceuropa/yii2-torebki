<?php

namespace backend\modules\cms\models;

use backend\modules\cms\Cms;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%i18n}}".
 *
 * @property string $var
 * @property string $lang
 * @property string $value
 */
class I18n extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_i18n}}';
    }

    public static function getLanguages()
    {
        return [
            'pl' => 'Polski',
        ];
    }

    public static function yesNo()
    {
        return [1 => 'Tak', 0 => 'Nie'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['var', 'language', 'value'], 'required'],
            [['var'], 'string', 'max' => 20],
            [['lang'], 'string', 'max' => 2],
            [['value'], 'string', 'max' => 512],
            [['var', 'language'], 'unique', 'targetAttribute' => ['var', 'language'], 'message' => 'The combination of Var and Lang has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'var' => Cms::t('app', 'Var'),
            'language' => Cms::t('app', 'Language'),
            'value' => Cms::t('app', 'Value'),
        ];
    }


}
