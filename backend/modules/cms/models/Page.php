<?php

namespace backend\modules\cms\models;

use dosamigos\transliterator\TransliteratorHelper;
use backend\modules\cms\Cms;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class Page
 * @property integer $id
 * @property string $language
 * @property integer $parent_id
 * @property integer $position
 * @property string $title
 * @property string $content
 * @property string $type_id
 * @property string $link
 * @property string $meta_title
 * @property string $meta_desc
 * @property integer $active
 * @property string $sort_tree
 * @property string $template
 *
 * @property Page $parent
 * @property Page[] $childs
 */
class Page extends ActiveRecord
{
    var $slug;

    const
        TYPE_HOME = 'home',
        TYPE_PAGE = 'page',
        TYPE_LINK = 'link';

    const
        TEMPLATE_DEFAULT = 'view',
        TEMPLATE_HOME = 'home',
        TEMPLATE_CONTACT = 'contact';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page}}';
    }

    /**
     * @param string $language
     * @param int $parent
     */
    public static function reSortPosition($language, $parent = 0)
    {
        /** @var Page[] $list */
        if ($list = Page::find()->where(['language' => $language, 'parent_id' => $parent])->orderBy('position asc')->all()) {
            $i = 2;
            foreach ($list as $l) {
                $l->position = $i;
                $l->update(false); //odpala after save i wyliczy sort_tree.
                if ($l->childs) {
                    Page::reSortPosition($language, $l->id);
                }
                $i += 2;
            }
        }
    }

    /**
     * @return array
     */
    public static function getFilter()
    {
        $list = Page::find()->select(['id', 'title'])->where(['parent_id' => 0])->orderBy('position asc')->asArray()->all();

        return ArrayHelper::map($list, 'id', 'title');
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'link',
                'ensureUnique' => true,
                'value' => function ($event) {
                    if (!empty($event->sender->link)) {
                        return $event->sender->link;
                    }

                    return Inflector::slug(TransliteratorHelper::process($event->sender->title, '', 'en'));
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type_id', 'active', 'template'], 'required'],
            [['parent_id', 'position', 'active'], 'integer'],
            [['parent_id'], 'default', 'value' => 0],
            [['template'], 'default', 'value' => self::TEMPLATE_DEFAULT],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 2],
            [['title'], 'string', 'max' => 255],
            [['type_id', 'sort_tree'], 'string', 'max' => 50],
            [['link', 'meta_title', 'meta_desc'], 'string', 'max' => 250],
            [['language', 'link'], 'unique', 'targetAttribute' => ['language', 'link'], 'message' => 'The combination of Link and Lang has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Cms::t('lbl', 'ID'),
            'language' => Cms::t('lbl', 'Language'),
            'parent_id' => Cms::t('lbl', 'Parent'),
            'position' => Cms::t('lbl', 'Position'),
            'title' => Cms::t('lbl', 'Title'),
            'content' => Cms::t('lbl', 'Content'),
            'type_id' => Cms::t('lbl', 'Type'),
            'link' => Cms::t('lbl', 'Link'),
            'meta_title' => Cms::t('lbl', 'Metatag - Title'),
            'meta_desc' => Cms::t('lbl', 'Metatag - Description'),
            'active' => Cms::t('lbl', 'Active'),
            'template' => Cms::t('lbl', 'Template'),
        ];
    }

    /**
     * @return string
     */
    public function getlink()
    {
        switch ($this->type_id) {
            case self::TYPE_HOME:
                return '/'.$this->language.'/page/index';
            case self::TYPE_LINK:
                return $this->link;
            default:
                return '/'.$this->language.'/page/'.$this->link;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Page::className(), ['id' => 'parent_id']);
    }

    /**
     * @return $this
     */
    public function getChilds()
    {
        return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position asc');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->position) {
            $this->position = $this->positionCalc();
        }
        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return int|mixed
     */
    public function positionCalc()
    {
        if ($this->id) {
            $model = Page::find()->where(['!=', 'id', $this->id])->andWhere(['language' => $this->language, 'parent_id' => $this->parent_id ? $this->parent_id : 0])->orderBy('position desc')->one();
        } else {
            $model = Page::find()->where(['language' => $this->language, 'parent_id' => $this->parent_id ? $this->parent_id : 0])->orderBy('position desc')->one();
        }

        if ($model) {
            return $model->position + 2;
        } else {
            return 2;
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->db->createCommand()->update(self::tableName(), ['sort_tree' => $this->getTreeSort()], ['id' => $this->id])->execute();
    }

    /**
     * @return mixed
     */
    private function getTreeSort()
    {
        $sort = [];

        $sort[] = str_pad($this->position, 3, '0', STR_PAD_LEFT);
        $el = $this;
        while ($el->parent_id) {
            $sort[] = str_pad($el->parent->position, 3, '0', STR_PAD_LEFT);
            $el = $el->parent;
        }

        $sort = array_reverse($sort);

        return implode('.', $sort);
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        $list = self::getTypes();

        return isset($list[$this->type_id]) ? $list[$this->type_id] : '?';
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PAGE => Cms::t('lbl', 'Page'),
            self::TYPE_HOME => Cms::t('lbl', 'Homepage'),
            self::TYPE_LINK => Cms::t('lbl', 'Link'),
        ];
    }

    /**
     * @return array
     */
    public static function getTemplates()
    {
        return [
            self::TEMPLATE_DEFAULT => Cms::t('lbl', 'Default'),
            self::TEMPLATE_HOME => Cms::t('lbl', 'Homepage'),
            self::TEMPLATE_CONTACT => Cms::t('lbl', 'Kontakt'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getTemplate()
    {
        $list = self::getTemplates();

        return isset($list[$this->template]) ? $list[$this->template] : '?';
    }
}
