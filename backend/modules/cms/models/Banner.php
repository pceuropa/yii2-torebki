<?php

namespace backend\modules\cms\models;

use backend\modules\cms\Cms;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%banners}}".
 *
 * @property string $id
 * @property string $title
 * @property string $link
 * @property string $photo
 * @property integer $active
 * @property string $language
 */
class Banner extends ActiveRecord {

    var $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%cms_banner}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'active'], 'required'],
            [['active', 'position'], 'integer'],
            [['title', 'link', 'photo', 'content', 'photo'], 'string', 'max' => 250],
            [['language'], 'string', 'max' => 2],
            [['position'], 'safe',],
            [['imageFile'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Cms::t('lbl', 'ID'),
            'language' => Cms::t('lbl', 'Language'),
            'title' => Cms::t('lbl', 'Title'),
            'link' => Cms::t('lbl', 'Link'),
            'photo' => Cms::t('lbl', 'Photo'),
            'active' => Cms::t('lbl', 'Active'),
            'content' => Cms::t('lbl', 'Content'),
        ];
    }

}
