<?php

namespace backend\modules\cms\controllers;

use backend\modules\cms\Cms;
use common\widgets\Alert;
use backend\modules\cms\models\Banner;
use backend\modules\cms\models\BannerSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class BannerController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $post = Yii::$app->request->post();
        if (isset($post['Banner'])) {
            foreach ($post['Banner'] as $key => $p) {
                if (isset($p['position'])) {

                    $model = $this->findModel($key);
                    $model->position = $p['position'];                   
                    $model->save(false);
                    
                }
            }
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate() {
        $model = new Banner();
        $model->language = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->imageFile = UploadedFile::getInstance($model, 'imageFile')) {
                $model->photo = $model->imageFile->baseName . '.' . $model->imageFile->extension;
                $model->imageFile->saveAs(Yii::getAlias('@frontend') . '/web/banner/' . $model->photo);
                $model->update(true, ['photo']);
            }
            Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->imageFile = UploadedFile::getInstance($model, 'imageFile')) {
                $model->photo = $model->imageFile->baseName . '.' . $model->imageFile->extension;
                $model->imageFile->saveAs(Yii::getAlias('@frontend') . '/web/banner/' . $model->photo);
                $model->update(true, ['photo']);
            }

            Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
