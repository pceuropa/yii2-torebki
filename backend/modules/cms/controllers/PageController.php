<?php

namespace backend\modules\cms\controllers;

use backend\modules\cms\Cms;
use common\widgets\Alert;
use backend\modules\cms\models\Page;
use backend\modules\cms\models\PageSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],

                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

            if (isset($_POST['apply'])) {
                return $this->redirect(['update', 'id' => $model->id]);
            }

            return $this->redirect(['index']);

        } else {

            return $this->render(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

            if (isset($_POST['apply'])) {
                return $this->redirect(['update', 'id' => $model->id]);
            }

            return $this->redirect(['index']);

        } else {
            return $this->render(
                'update',
                [
                    'model' => $model,
                ]
            );
        }
    }

    public function actionPosition($id, $position)
    {
        if ($model = Page::findOne($id)) {
            $model->position = $position;
            $model->update(false);
            Page::reSortPosition($model->language);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->childs) {
            Alert::add(Cms::t('msg', 'First delete all childs'), Alert::TYPE_SUCCESS);
        } else {
            $this->findModel($id)->delete();
            Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
