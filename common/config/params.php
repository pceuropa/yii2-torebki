<?php
return [
    'adminEmail' => 'info@pceuropa.net',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
