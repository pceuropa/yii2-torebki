<?php

namespace frontend\modules\cms\controllers;

use frontend\modules\cms\models\Page;
use frontend\modules\cms\models\ContactForm;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * @return mixed
     */
    public function actionView($link)
    {
        $model = $this->findModel($link);

        $cmodel = new ContactForm();

        if ($cmodel->load(Yii::$app->request->post()) && $cmodel->contact()) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }


        return $this->render($model->template, [
            'model' => $model,
            'cmodel'=>$cmodel,
        ]);
    }


    /**
     * @param string $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::find()->where(['link' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
