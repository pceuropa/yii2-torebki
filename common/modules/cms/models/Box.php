<?php

namespace common\modules\cms\models;

use common\helpers\LoremIpsum;
use common\modules\cms\Cms;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%boxes}}".
 *
 * @property string $id
 * @property string $var
 * @property string $language
 * @property string $title
 * @property string $content
 * @property string $link
 * @property string $meta_title
 * @property string $meta_desc
 */
class Box extends ActiveRecord
{
    var $slug;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_box}}';
    }

    public function getLink()
    {
        return '/' . $this->language . '/box/' . $this->link;
    }

    public static  function forceGet($boxName, $defaultLength = 255)
    {
        if ($box = Box::find()->where(['var' => $boxName])->one()) {
            return $box;
        }
        else
        {
            $box = new Box();
            $box->title = $box->var = $boxName;
            $box->language = Yii::$app->language;

            $loremIpsum = new LoremIpsum();
            $loremIpsum->setLength($defaultLength);
            $box->content = $loremIpsum->get();
            $box->save(false);
        }

        return $box;
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'link',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['var'], 'string', 'max' => 80],
            [['language'], 'string', 'max' => 2],
            [['title'], 'string', 'max' => 255],
            [['link', 'meta_title', 'meta_desc'], 'string', 'max' => 250],
            [['var', 'language'], 'unique', 'targetAttribute' => ['var', 'language'], 'message' => 'The combination of Var and Lang has already been taken.'],
            [['link', 'language'], 'unique', 'targetAttribute' => ['link', 'language'], 'message' => 'The combination of Var and Lang has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Cms::t('lbl', 'ID'),
            'var' => Cms::t('lbl', 'Name of variable'),
            'language' => Cms::t('lbl', 'Language'),
            'title' => Cms::t('lbl', 'Title'),
            'content' => Cms::t('lbl', 'Content'),
            'link' => Cms::t('lbl', 'Link'),
            'meta_title' => Cms::t('lbl', 'Metatag - Title'),
            'meta_desc' => Cms::t('lbl', 'Metatag - Description'),
        ];
    }
}
