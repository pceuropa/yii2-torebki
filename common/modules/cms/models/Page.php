<?php

namespace common\modules\cms\models;

use dosamigos\transliterator\TransliteratorHelper;
use common\modules\cms\Cms;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class Page
 * @property integer $id
 * @property string $language
 * @property integer $parent_id
 * @property integer $position
 * @property string $title
 * @property string $content
 * @property string $type_id
 * @property string $link
 * @property string $meta_title
 * @property string $meta_desc
 * @property integer $active
 * @property string $sort_tree
 * @property string $template
 *
 * @property Page $parent
 * @property Page[] $childs
 */
class Page extends ActiveRecord
{
    var $slug;

    const
        TYPE_HOME = 'home', TYPE_PAGE = 'page', TYPE_LINK = 'link';

    const
        TEMPLATE_DEFAULT = 'view', TEMPLATE_HOME = 'home', TEMPLATE_HOSTING = 'hosting-package';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page}}';
    }


    /**
     * @return string
     */
    public function getlink()
    {
        switch ($this->type_id) {
            case self::TYPE_HOME:
                return '/' . $this->language . '/page/index';
            case self::TYPE_LINK:
                return $this->link;
            default:
                return '/' . $this->language . '/page/' . $this->link;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Page::className(), ['id' => 'parent_id']);
    }

    public static function getParents()
    {
        return Page::find()->where(['active'=>1, 'parent_id'=>0])->orderBy('position asc')->all();
    }

    /**
     * @return $this
     */
    public function getChilds()
    {
        return $this->hasMany(Page::className(), ['parent_id' => 'id'])->andWhere(['active' => 1])->orderBy('position asc');
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        $list = self::getTypes();

        return isset($list[$this->type_id]) ? $list[$this->type_id] : '?';
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PAGE => Cms::t('lbl', 'Page'),
            self::TYPE_HOME => Cms::t('lbl', 'Homepage'),
            self::TYPE_LINK => Cms::t('lbl', 'Link'),
        ];
    }

    /**
     * @return array
     */
    public static function getTemplates()
    {
        return [
            self::TEMPLATE_DEFAULT => Cms::t('lbl', 'Default'),
            self::TEMPLATE_HOME => Cms::t('lbl', 'Homepage'),
            self::TEMPLATE_HOSTING => Cms::t('lbl', 'Hosting Package'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getTemplate()
    {
        $list = self::getTemplates();

        return isset($list[$this->template]) ? $list[$this->template] : '?';
    }
}
