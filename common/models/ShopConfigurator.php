<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shop_configurator".
 *
 * @property integer $id
 * @property string $type
 * @property string $value
 * @property double $price
 * @property string $product_id
 */
class ShopConfigurator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_configurator';
    }


    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    public function getName()
    {
        return '['.$this->value.'] '.$this->value.' '.$this->price;
    }

    public static function getFilterList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'value');
    }

    public function product()
    {
        if ($this->product_id)
        {
            $tmp = Product::findOne($this->product_id);
            if($tmp)
            {
                $this->price = $tmp->priceBrutto();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'value', 'price'], 'required'],
            [['price'], 'number'],
            [['product_id'], 'integer'],
            [['type'], 'string', 'max' => 20],
            [['value'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Typ'),
            'value' => Yii::t('app', 'Wartość'),
            'price' => Yii::t('app', 'Cena'),
            'product_id' => Yii::t('app', 'Produkt'),
            'product_name' => 'Nazwa produktu',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['product_id' => 'id']);
    }

    public static function getTypeFilterList()
    {
        return ['hdd' => 'Dysk Twardy', 'ram' => 'Pamięć RAM', 'system' => 'System operacyjny', 'kabel' => 'Przewód', 'klawiatura' => 'Klawiatura', 'mysz' => 'Myszka'];
    }
}
