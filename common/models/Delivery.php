<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "delivery".
 *
 * @property integer $id
 * @property string $name
 * @property string $price
 * @property integer $pobranie
 * @property string $free
 * @property integer $active
 * @property string $info
 */
class Delivery extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'delivery';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    public static function getFilterList() {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['name', 'price', 'free',], 'required'],
                [['price', 'free'], 'number'],
                [['pobranie', 'active'], 'integer'],
                [['payment_method'], 'safe'],
                [['name', 'info'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Nazwa'),
            'price' => Yii::t('app', 'Cena'),
            'pobranie' => Yii::t('app', 'Pobranie'),
            'free' => Yii::t('app', 'Darmowa wysyłka'),
            'active' => Yii::t('app', 'Aktywna'),
            'info' => Yii::t('app', 'Info'),
            'payment_method' => Yii::t('app', 'Metoda płatności'),
        ];
    }
    
    public function getPaymentMethodNames() {
        
        return print_r(unserialize($this->payment_method), true);
        
    }

}
