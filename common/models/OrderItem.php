<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order_item".
 *
 * @property string $id
 * @property string $order_id
 * @property string $product_id
 * @property string $name
 * @property string $price_one
 * @property string $quantity
 * @property string $rozmiar
 * @property string $rozmiar_name
 * @property integer $stocks
 * @property string $status
 * @property string $color
 * @property string $color_name
 * @property string $code
 * @property string $options
 */
class OrderItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    public static function getFilterList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'name', 'price_one', 'quantity'], 'required'],
            [['order_id', 'product_id', 'quantity', 'stocks'], 'integer'],
            [['price_one'], 'number'],
            [['options'], 'string'],
            [['name', 'rozmiar', 'rozmiar_name', 'color', 'color_name'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 5],
            [['code'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Zamówienie'),
            'product_id' => Yii::t('app', 'Produkt'),
            'name' => Yii::t('app', 'Nazwa'),
            'price_one' => Yii::t('app', 'Cena za szt.'),
            'quantity' => Yii::t('app', 'Ilość'),
            'options' => Yii::t('app', 'Opcje'),
        ];
    }
}
