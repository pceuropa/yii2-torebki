<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\{Product};

/**
 * This is the model class for table "shop_product_attribute".
 * @property string $id
 * @property string $product_id
 * @property integer $attribute_id
 * @property string $value
 * @property string $unit
 * @property mixed category_id
 */
class ShopProductAttribute extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'shop_product_attribute';
    }

    /**
     * filter only min 2 elements
     * @param string $arg
     * @return void
     */
    public static function getCategoryFilters2($id, $quantity=0) {
      $out = [];
      $posts = Yii::$app->db->createCommand('
        SELECT DISTINCT pa.attribute_id, pa.value, a.name 
        FROM shop_product_attribute as pa INNER JOIN shop_attribute as a ON pa.attribute_id = a.id 
        WHERE pa.product_id in (select id from shop_product where category_id in (:id) and active = 1 and quantity > :quantity)')
        ->bindValue(':id', implode(', ', $id))
        ->bindValue(':quantity', $quantity)
        ->queryAll();


      foreach ($posts as $key => $value) {
        $out[$value['attribute_id']] = [];
      }
      foreach ($posts as $key => $value) {
        array_push($out[$value['attribute_id']], ['value' => $value['value'], 'name' => $value['name']]);
      }

      return $out;
      return array_filter($out, function($k){
            return count($k) > 1;
      });
    }

    public static function getCategoryFilters($id, $ids) {
        $out = [];
        $category = Category::findOne($id);
        if( $filters = $category->getFilters() )
        {

            foreach($filters as $filter)
            {
                $out[$filter] = ShopProductAttribute::find()->select(['value', 'name'])->where(['category_id'=>$ids,'attribute_id'=>$filter])->groupBy(['value'])->asArray()->all();
            }

        }

        return $out;
    }

    /**
     * __toString()
     * @return string
     */
    public function __toString() {
        return $this->id;
    }


    public static function getFilterListProduct($id) {
        return ShopProductAttribute::find()->where(['product_id'=>$id])->asArray()->indexBy('attribute_id')->all();
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'attribute_id', 'value','category_id'], 'required'],
            [['product_id', 'attribute_id'], 'integer'],
            [['value'], 'string', 'max' => 100],
            [['unit'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'product_id' => 'Product',
            'attribute_id' => 'Attribute',
            'value' => 'Value',
            'unit' => 'Unit',
        ];
    }

    public function getAttributeName() {
        return $this->hasOne(ShopAttribute::className(), ['id' => 'attribute_id']);
    }

    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

}
