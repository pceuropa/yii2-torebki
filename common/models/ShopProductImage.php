<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
/**
 * This is the model class for table "shop_product_image".
 *
 * @property string $id
 * @property string $product_id
 * @property string $photo
 * @property string $position
 */
class ShopProductImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_product_image';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    public static function getFilterList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'id');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'photo'], 'required'],
            [['product_id', 'position'], 'integer'],
            [['photo'], 'string', 'max' => 80],
        ];
    }

    public function getUrl($size)
    {
        $whSize = explode('x',$size);
        $path = Yii::getAlias('@frontend/web/products') . DIRECTORY_SEPARATOR;
        if(!file_exists($path.$size.'___'.$this->photo) and file_exists($path.$this->photo))
        {
            try {
                Image::getImagine()->open($path . $this->photo)->thumbnail(new Box($whSize[0], $whSize[1]))->save($path . $size . '___' . $this->photo, ['quality' => 90]);
            }
            catch(\Exception $e)
            {

            }
           /* Image::thumbnail($path.$this->photo, $whSize[0],$whSize[1])
                ->save($path.$size.'___'.$this->photo, ['quality' => 90]);*/
        }
        return Yii::$app->setting->get('frontUrl').'/products/'.$size.'___'.$this->photo;
    }

    public function deleteFiles()
    {
        $files = glob(Yii::getAlias('@frontend/web/products') . DIRECTORY_SEPARATOR.'*___'.$this->photo);
        foreach($files as $file)
            @unlink($file);

        @unlink(Yii::getAlias('@frontend/web/products') . DIRECTORY_SEPARATOR.$this->photo);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product',
            'photo' => 'Photo',
            'position' => 'Position',
        ];
    }
}
