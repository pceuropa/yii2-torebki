<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "voucher_codes".
 *
 * @property string $id
 * @property string $voucher_id
 * @property string $code
 * @property string $order_id
 */
class VoucherCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voucher_codes';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    public static function getFilterList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'id');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['voucher_id', 'code'], 'required'],
            [['voucher_id', 'order_id'], 'integer'],
            [['code'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'voucher_id' => 'Voucher',
            'code' => 'Code',
            'order_id' => 'Order',
        ];
    }

    public function getVoucher()
    {
        return $this->hasOne(Voucher::className(),['id'=>'voucher_id']);
    }
}
