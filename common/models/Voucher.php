<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "voucher".
 *
 * @property string $id
 * @property string $price
 * @property integer $percent
 * @property string $code
 * @property string $desc
 * @property string $quantity
 * @property string $date_from
 * @property string $date_to
 * @property integer $client
 * @property string $mail
 */
class Voucher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voucher';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    public static function getFilterList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'id');
    }

    public function getCodes()
    {
        return $this->hasMany(VoucherCode::className(),['voucher_id'=>'id']);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'number'],
            [['percent', 'quantity', 'client'], 'integer'],
            [['code', 'desc', 'quantity', 'date_from', 'date_to'], 'required'],
            [['price'], 'checkDiscount', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['date_from', 'date_to'], 'safe'],
            [['code', 'desc'], 'string', 'max' => 255],
            [['mail'], 'string', 'max' => 120],
        ];
    }

    public function checkDiscount()
    {
        if($this->price and $this->percent)
        {
            $this->addError('percent','Możesz ustawić rabat albo kwotowy albo procentowy');
        }

        if(!$this->price and !$this->percent)
        {

            $this->addError('percent','Proszę ustawić rabat albo kwotowy albo procentowy');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Cena',
            'percent' => 'Procent',
            'code' => 'Kod',
            'desc' => 'Opis',
            'quantity' => 'Ilość',
            'date_from' => 'Ważny od',
            'date_to' => 'Ważny do',
            'client' => 'Klient',
            'mail' => 'email',
        ];
    }

    public static function generateNewsletterVoucher()
    {
        $voucher = false;
        if ($voucher = Voucher::find()->where('code="NWS-LET" and date_from<=CURDATE() and date_to>=CURDATE()')->one()) {

            $vc = new VoucherCode();
            $vc->voucher_id = $voucher->id;
            $vc->code = $voucher->code . '-' . $voucher->random();
            while ($vc->validate() == false) {
                $vc->code = $voucher->code . '-' . $voucher->random();
            }
            $vc->save(false);
            return $vc->code;
        }

        return false;
    }


    public function generateCodes($i)
    {
        $kody = array();
        while ($i > 0) {
            $vc = new VoucherCode();
            $vc->voucher_id = $this->id;
            $vc->code = $this->code . '-' . $this->random();

            while ($vc->validate() == false) {
                $vc->code = $this->code . '-' . $this->random();
            }
            $kody[] = $vc->code;
            $vc->save();
            $i--;
        }
        return $kody;
    }

    public function random($length = 3)
    {
        $chars = 'QWERTYUIOPLKJHGFDSAMNBVCXZ0987654321';
        $c = strlen($chars) - 1;
        $result = '';
        for ($p = 0; $p < $length; $p++) {
            $result .= $chars[mt_rand(0, $c)];
        }

        return $result;
    }

    public function sendVouchers($kody)
    {

        if ($this->client > 0) {
            $cl = Client::findOne($this->client);
            $mail = $cl->c_email;
        } elseif ($this->mail) {
            $mail = $this->mail;
        } else {
            return true;
        } //jezeli nie ma słac


        $message = '<table cellspacing="0" cellpadding="0" border="0" >
                        <tr>
                                                <td style="background:#fff; width:500px" >
                                                <h3>Kupon rabatowy</h3>
                                                <div style="clear:both; height:10px"></div>
                                                <div style="padding:10px">
                        Otrzymujesz od nas kupon rabatowy w wysokości  ' . ($this->price > 0 ? $this->price . ' zł' : $this->percent . ' %') . ' na dowolne produkty w naszym sklepie internetowym.
                        Wystarczy, że w koszyku Twoich zakupów wprowadzisz ' . (count($kody) > 1 ? 'jeden z poniższych kodów' : 'poniższy kod') . ':<br/>
                        <ul>
                        ';

        foreach ($kody as $k) {
            $message .= '<li>' . $k . '</li>';
        }


        $message .= '
                        </ul>

                        </div>
                        Nie zwklekaj, dokonaj zakupów już dzisiaj. Twój kupon rabatowy ważny jest do ' . $this->date_to . '<br/>
                        <a href="http://slaskiecentrumkomputerowe.com.pl/">www.slaskiecentrumkomputerowe.com.pl</a>
                        </td></tr></table>
                        ';
        Controller::sendmail('Otrzymałeś kod rabatowy na zakupy w naszym sklepie', $message, $mail);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $ile = VoucherCode::find()->where(['voucher_id' => $this->id])->count();
        if ($this->quantity and ($ile < $this->quantity)) {
            $this->generateCodes($this->quantity - $ile);
        }

    }
}
