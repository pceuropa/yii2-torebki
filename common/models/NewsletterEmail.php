<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%cms_newsletter_email}}".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $mail
 * @property integer $active
 */
class NewsletterEmail extends \yii\db\ActiveRecord
{
    var $terms, $action;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_newsletter_email}}';
    }


    public static function getFilterList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'mail');
    }

    public static function hash($element)
    {
        return md5('dsg4' . $element . 'dgfa4y35');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail', 'terms'], 'required'],
            [['group_id', 'active'], 'integer'],
            [['mail'], 'unique'],
            [['mail'], 'email'],
            [['mail'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Grupa',
            'mail' => 'Adres e-mail',
            'active' => 'Aktywny',
            'terms' => 'Akcteptuję regulami'
        ];
    }
}
