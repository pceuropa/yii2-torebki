<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'status', 'fv'], 'integer'],
            [['products_price', 'transport_price', 'payment_price', 'rabat', 'voucher_price', 'voucher_percent'], 'number'],
            [['transport_name', 'transport_number', 'payment_name', 'payment_number', 'notice', 'date', 'fv_nip', 'fv_company', 'fv_address', 'fv_postcode', 'fv_city', 'fv_country', 'd_name', 'd_company', 'd_address', 'd_postcode', 'd_city', 'd_country', 'd_phone', 'c_name', 'c_surname', 'c_email', 'c_phone', 'rabat_name', 'voucher_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => 'desc']],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'status' => $this->status,
            'products_price' => $this->products_price,
            'transport_price' => $this->transport_price,
            'payment_price' => $this->payment_price,
            'date' => $this->date,
            'fv' => $this->fv,
            'rabat' => $this->rabat,
            'voucher_price' => $this->voucher_price,
            'voucher_percent' => $this->voucher_percent,
        ]);

        $query->andFilterWhere(['like', 'transport_name', $this->transport_name])->andFilterWhere(['like', 'transport_number', $this->transport_number])->andFilterWhere(['like', 'payment_name', $this->payment_name])->andFilterWhere(['like', 'payment_number', $this->payment_number])->andFilterWhere(['like', 'notice', $this->notice])->andFilterWhere(['like', 'fv_nip', $this->fv_nip])->andFilterWhere(['like', 'fv_company', $this->fv_company])->andFilterWhere(['like', 'fv_address', $this->fv_address])->andFilterWhere(['like', 'fv_postcode', $this->fv_postcode])->andFilterWhere(['like', 'fv_city', $this->fv_city])->andFilterWhere(['like', 'fv_country', $this->fv_country])->andFilterWhere(['like', 'd_name', $this->d_name])->andFilterWhere([
                'like',
                'd_company',
                $this->d_company
            ])->andFilterWhere(['like', 'd_address', $this->d_address])->andFilterWhere(['like', 'd_postcode', $this->d_postcode])->andFilterWhere(['like', 'd_city', $this->d_city])->andFilterWhere(['like', 'd_country', $this->d_country])->andFilterWhere(['like', 'd_phone', $this->d_phone])->andFilterWhere(['like', 'c_name', $this->c_name])->andFilterWhere(['like', 'c_surname', $this->c_surname])->andFilterWhere(['like', 'c_email', $this->c_email])->andFilterWhere(['like', 'c_phone', $this->c_phone])->andFilterWhere(['like', 'rabat_name', $this->rabat_name])->andFilterWhere(['like', 'voucher_name', $this->voucher_name]);

        return $dataProvider;
    }
}
