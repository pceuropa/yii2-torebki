<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "client".
 *
 * @property string $id
 * @property string $login
 * @property string $date
 * @property integer $fv
 * @property string $fv_nip
 * @property string $fv_company
 * @property string $fv_address
 * @property string $fv_postcode
 * @property string $fv_city
 * @property string $fv_country
 * @property string $d_company
 * @property string $d_name
 * @property string $d_address
 * @property string $d_postcode
 * @property string $d_city
 * @property string $d_country
 * @property string $d_phone
 * @property string $c_name
 * @property string $c_email
 * @property string $c_phone
 * @property string $password
 * @property integer $rabat
 */
class Client extends \yii\db\ActiveRecord implements IdentityInterface {

    const SCENARIO_REGISTER = 'register';
    const SCENARIO_NO_ACCOUNT = 'no-account';

    var $passwordNew, $passwordConfirm;
    var $term1, $term2, $newsletter;
    var $deliveryCopyFromClient;
    var $fvCopyFromClient;
    var $fvCopyFromDelivery;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'client';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString() {
        return $this->c_name;
    }

    public function getToString() {
        return $this->c_name;
    }

    public static function getFilterList() {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'id');
    }

    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['auth_key'], 'string', 'max' => 32],
            [['date'], 'safe'],
            [['d_name', 'd_address', 'd_postcode', 'd_city', 'd_country', 'd_phone', 'd_email',
            'c_name', 'c_email', 'c_phone', 'c_address', 'c_country', 'c_postcode', 'c_city'], 'required'],
            [['fv', 'rabat', 'newsletter'], 'integer'],
            [
                ['c_email'],
                function ($attr) {
            if (Client::find()->andWhere(['c_email' => $this->c_email])->andWhere(['is not', 'password', null])->limit(1)->one()) {
                $this->addError('c_email', 'Na podany adres email zostało już założone konto');
            }
        },
                'on' => self::SCENARIO_REGISTER
            ],
            [['c_email'], 'email'],
            [['login', 'passwordNew', 'passwordConfirm'], 'string', 'max' => 100],
            [['fv_nip'], 'string', 'max' => 13],
            [['fv_company', 'fv_address', 'fv_city', 'fv_country', 'd_company', 'd_name', 'd_address', 'd_city', 'd_country', 'd_phone', 'c_name', 'c_email', 'c_phone'], 'string', 'max' => 255],
            [['fv_postcode', 'd_postcode'], 'string', 'max' => 6],
            [['password'], 'string', 'max' => 32],
            [['passwordNew', 'passwordConfirm'], 'required', 'on' => self::SCENARIO_REGISTER],
            [['term1', 'term2'], 'required'],
            ['term1', 'required', 'requiredValue' => 1, 'message' => 'Musisz zaakceptowac regulamin'],
            ['term2', 'required', 'requiredValue' => 1, 'message' => 'Musisz zaakceptowac zgodę na przetwarzanie danych'],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_NO_ACCOUNT] = ['d_postcode', 'fv_company', 'fv_address', 'fv_city', 'fv_country', 'd_company', 'd_name', 'd_address', 'd_city', 'd_country', 'd_phone', 'c_name', 'c_email', 'c_phone', 'term1', 'term2'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Login'),
            'date' => Yii::t('app', 'Data rejestracji'),
            'fv' => Yii::t('app', 'Proszę o dostarczenie faktury VAT'),
            'fv_nip' => Yii::t('app', ' Nip'),
            'fv_company' => Yii::t('app', 'Firma'),
            'fv_address' => Yii::t('app', 'Ulica nr'),
            'fv_postcode' => Yii::t('app', 'Kod pocztowy'),
            'fv_city' => Yii::t('app', 'Miejscowość'),
            'fv_country' => Yii::t('app', 'Kraj'),
            'd_company' => Yii::t('app', 'Firma'),
            'd_name' => Yii::t('app', 'Imię i nazwisko'),
            'd_address' => Yii::t('app', 'Ulica nr'),
            'c_address' => Yii::t('app', 'Ulica nr'),
            'd_postcode' => Yii::t('app', 'Kod pocztowy'),
            'c_postcode' => Yii::t('app', 'Kod pocztowy'),
            'd_city' => Yii::t('app', 'Miejscowość'),
            'c_city' => Yii::t('app', 'Miejscowość'),
            'd_country' => Yii::t('app', 'Kraj'),
            'c_country' => Yii::t('app', 'Kraj'),
            'd_phone' => Yii::t('app', 'Telefon'),
            'c_name' => Yii::t('app', 'Imię i nazwisko'),
            'c_email' => $this->scenario == self::SCENARIO_REGISTER ? Yii::t('app', 'Login / Email') : 'Email',
            'd_email' => 'Email',
            'c_phone' => Yii::t('app', 'Telefon'),
            'password' => Yii::t('app', 'Hasło'),
            'rabat' => Yii::t('app', 'Rabat'),
            'passwordNew' => 'Hasło',
            'passwordConfirm' => 'Potwierdź hasło',
            'term1' => 'Akceptacja regulaminu',
            'term2' => 'Zgoda na przetwarzanie danych',
            'newsletter' => 'Zgodna na newsletter',
            'deliveryCopyFromClient' => 'użyj danych z danych zamawiającego',
            'fvCopyFromClient' => 'użyj danych z danych zamawiającego',
            'fvCopyFromDelivery' => 'użyj danych z danych do wysyłki',
        ];
    }

    public function sendConfirmMail() {
//        $state = Yii::$app->setting->mailer()->compose('confirm-mail', ['model' => $this])
//                ->setTo($this->c_email)
//                ->setFrom([Yii::$app->setting->get('smtp_login') => $_SERVER['HTTP_HOST']])
//                ->setSubject('Rejestracja w serwisie')
//                ->send();
        $state = Yii::$app->mailer->compose('confirm-mail', [ 'model' => $this])
                ->setTo($this->c_email)
                ->setFrom([Yii::$app->params['adminEmail'] => $_SERVER['HTTP_HOST']])
                ->setSubject('Rejestracja w serwisie')
                ->send();
        //var_dump($state);
        //exit(0);

        if ($state) {
            return true;
        } else {
            return false;
        }
    }

    public function sendResetEmail() {
        $link = 'http://' . $_SERVER['HTTP_HOST'] . '/panel/users/reset?token=' . $this->password_reset_token;
//        $state = Settings::mailer()->compose('reset-password', ['link' => $link])
//                ->setTo($this->email)
//                ->setFrom([Settings::g('smtp_login', 'form') => $_SERVER['HTTP_HOST']])
//                ->setSubject(Yii::t('app', 'Reset Password'))
//                ->send();
        $state = Yii::$app->mailer->compose('reset-password', ['link' => $link])
                ->setTo($this->email)
                ->setFrom([Yii::$app->params['adminEmail'] => $_SERVER['HTTP_HOST']])
                ->setSubject(Yii::t('app', 'Reset Password'))
                ->send();
        if ($state) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {

        return static::findOne(['id' => $id, 'active' => 1]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::find()->andWhere(['c_email' => $username, 'active' => 1])->andWhere(['is not', 'password', null])->limit(1)->one();
    }

    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return md5($password) == $this->password;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public function setPassword($password) {
        $this->password = md5($password);
    }

}
