<?php

namespace common\models;

use frontend\models\Basket;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property string $id
 * @property string $client_id
 * @property integer $status
 * @property string $products_price
 * @property string $transport_price
 * @property string $transport_name
 * @property string $transport_number
 * @property string $payment_price
 * @property string $payment_name
 * @property string $payment_number
 * @property string $notice
 * @property string $date
 * @property integer $fv
 * @property string $fv_nip
 * @property string $fv_company
 * @property string $fv_address
 * @property string $fv_postcode
 * @property string $fv_city
 * @property string $fv_country
 * @property string $d_name
 * @property string $d_company
 * @property string $d_address
 * @property string $d_postcode
 * @property string $d_city
 * @property string $d_country
 * @property string $d_phone
 * @property string $c_name
 * @property string $c_surname
 * @property string $c_email
 * @property string $c_phone
 * @property string $rabat_name
 * @property string $rabat
 * @property string $voucher_price
 * @property string $voucher_percent
 * @property string $voucher_name
 */
class Order extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order';
    }

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString() {
        return $this->id;
    }

    public static function getFilterList() {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'id');
    }

    public function sig()
    {
        //sig = md5 ( pos_id + pay_type + session_id + pos_auth_key + amount + desc + desc2 + trsDesc + order_id + first_name + last_name + street + street_hn + street_an
        //	+ city + post_code + country + email + phone + language + client_ip + ts + key1 )
        $sig = md5(Yii::$app->setting->get('payu_pos_id').$this->id
            .Yii::$app->setting->get('payu_pos_auth_key')
            .($this->toPay()*100)
            ."Zamowienie nr #{$this->id}"

            .$_SERVER['REMOTE_ADDR']
            .strtotime($this->date)
            .Yii::$app->setting->get('payu_key')
        );
        return $sig;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['client_id', 'status', 'fv'], 'integer'],
            [['products_price', 'transport_price', 'transport_name', 'fv', 'd_name', 'd_address', 'd_postcode', 'd_city', 'd_country', 'd_phone', 'c_name', 'c_email', 'c_phone',], 'required'],
            [['products_price', 'transport_price', 'payment_price', 'rabat', 'voucher_price', 'voucher_percent'], 'number'],
            [['date'], 'safe'],
            [['transport_name', 'transport_number', 'payment_name', 'payment_number', 'notice', 'fv_company', 'fv_address', 'fv_city', 'fv_country', 'd_name', 'd_company', 'd_address', 'd_city', 'd_country', 'd_phone', 'c_name', 'c_surname', 'c_email', 'c_phone', 'rabat_name', 'voucher_name'], 'string', 'max' => 255],
            [['fv_nip'], 'string', 'max' => 13],
            [['fv_postcode', 'd_postcode'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'client_name' => 'Klient',
            'statusName' => 'Status',
            'id' => Yii::t('app', 'ID'),
            'client_id' => Yii::t('app', 'Klient'),
            'status' => Yii::t('app', 'Status'),
            'products_price' => Yii::t('app', 'Wartość produktów'),
            'transport_price' => Yii::t('app', 'Koszt wysyłki'),
            'transport_name' => Yii::t('app', 'Sposób wysyłki'),
            'transport_number' => Yii::t('app', 'Numer przesyłi'),
            'payment_price' => Yii::t('app', 'Koszt płatności'),
            'payment_name' => Yii::t('app', 'Płatność'),
            'payment_number' => Yii::t('app', 'Numer płatności'),
            'notice' => Yii::t('app', 'Uwagi'),
            'date' => Yii::t('app', 'Data'),
            'fv' => Yii::t('app', 'Proszę o dostarczenie faktury VAT'),
            'fv_nip' => Yii::t('app', 'Nip'),
            'fv_company' => Yii::t('app', 'Nazwa firmy'),
            'fv_address' => Yii::t('app', 'Adres'),
            'fv_postcode' => Yii::t('app', 'Kod pocztowy'),
            'fv_city' => Yii::t('app', 'Miejscowość'),
            'fv_country' => Yii::t('app', 'Kraj'),
            'd_name' => Yii::t('app', 'Imię i naziwsko'),
            'd_company' => Yii::t('app', 'Nazwa firmy'),
            'd_address' => Yii::t('app', 'Adres'),
            'd_postcode' => Yii::t('app', 'Kod pocztowy'),
            'd_city' => Yii::t('app', 'Miejscowość'),
            'd_country' => Yii::t('app', 'Kraj'),
            'd_phone' => Yii::t('app', 'Telefon'),
            'c_name' => Yii::t('app', 'Imię i nazwisko'),
            'c_email' => Yii::t('app', 'Email'),
            'c_phone' => Yii::t('app', 'Telefon'),
            'rabat_name' => Yii::t('app', 'Rabat'),
            'rabat' => Yii::t('app', 'Rabat'),
            'voucher_price' => Yii::t('app', 'Voucher PLN'),
            'voucher_percent' => Yii::t('app', 'Voucher %'),
            'voucher_name' => Yii::t('app', 'Voucher Nazwa'),
        ];
    }

    public function getClient() {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    public function getStatusName() {
        return Status::getStatus($this->status);
    }

    public function getItems() {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    public function copyClient() {
        if (Yii::$app->user->isGuest) {
            $basket = new Basket();
            $user = $basket->getClient();
        } else {
            $user = Yii::$app->user->identity;
            $this->client_id = $user->id;
        }


        $attributes = [
            'fv', 'fv_nip', 'fv_company', 'fv_address', 'fv_postcode', 'fv_city', 'fv_country',
            'd_name', 'd_company', 'd_address', 'd_postcode', 'd_city', 'd_country', 'd_phone',
            'c_name', 'c_email', 'c_phone',
        ];
        foreach ($attributes as $attribute) {
            $this[$attribute] = $user[$attribute];
        }
    }

    public function toPay() {
        return sprintf("%.2f", $this->transport_price + $this->products_price);
    }

    public function sendConfirmMail() {
//        return Yii::$app->setting->mailer()->compose('order-html', [ 'model' => $this])
//                        ->setFrom([ Yii::$app->setting->get('smtp_login') => $_SERVER['HTTP_HOST']])
//                        ->setTo($this->c_email)
//                        ->setBcc(Yii::$app->setting->get('smtp_login'))
//                        ->setSubject('Potwierdzenie zamówienia: # ' . $this->id)->send();

        return Yii::$app->mailer->compose('order-html', [ 'model' => $this])
                        ->setTo($this->c_email)
                        ->setFrom([Yii::$app->params['adminEmail'] => $_SERVER['HTTP_HOST']])
                        ->setBcc(Yii::$app->params['adminEmail'])
                        ->setSubject('Potwierdzenie zamówienia: # ' . $this->id)
                        ->send();
    }

    public function sendStatusMail() {
        $status = Status::find()->where(['status_id' => $this->status])->one();
        if ($status->mail) {
            $status->template = str_ireplace('{DATA}', date("Y-m-d"), $status->template);
            $status->template = str_ireplace('{ID}', $this->id, $status->template);

//            Yii::$app->setting->mailer()->compose('status-html',[ 'content' => $status->template, 'model'=> $this])
//                ->setFrom([ Yii::$app->setting->get('smtp_login') => $_SERVER['HTTP_HOST']])
//                ->setTo($this->c_email)
//                ->setSubject('Zmiana statusu zamówienia: # ' . $this->id)->send();

            return Yii::$app->mailer->compose('status-html', [ 'content' => $status->template, 'model' => $this])
                            ->setTo($this->c_email)
                            ->setFrom(Yii::$app->params['adminEmail'])
                            ->setSubject('Zmiana statusu zamówienia: # ' . $this->id)
                            //->setTextBody($this->body . ' Telefon: ' . $this->phone . ', Adress e-mail: ' . $this->email)
                            ->send();
        }
    }

}
