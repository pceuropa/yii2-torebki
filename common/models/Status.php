<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shop_status".
 *
 * @property string $id
 * @property integer $status_id
 * @property string $template
 * @property integer $mail
 * @property integer $sms
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_status';
    }
    
    /**
     * __toString()
     *
     * @return string
     */
    public function getToString()
    {
        return $this->getStatus($this->status_id);
    }

    public static function getFilterList()
    {
        return self::getStatus();
    }


    public static function getStatus($el = false) {
        $a = array(
            0 => 'Nowe zamówienie',
            5 => 'W trakcie realizacji',
            10 => 'Gotowe do wysłania/wydania',
            15 => 'Zrealizowane / wysłane',
            30 => 'Zwrot',
            50 => 'Anulowane',
        );



        if ($el !== false)
            return isset($a[$el])?$a[$el]:'?';
        else
            return $a;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id', 'template'], 'required'],
            [['status_id', 'mail', 'sms'], 'integer'],
            [['template'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status_id' => Yii::t('app', 'Status'),
            'template' => Yii::t('app', 'Szablon'),
            'mail' => Yii::t('app', 'Mail'),
            'sms' => Yii::t('app', 'Sms'),
            'toString' => 'Nazwa',
        ];
    }
}
