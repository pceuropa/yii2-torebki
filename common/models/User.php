<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $default_materials
 * @property string $discount
 * @property string $modified_metal_price
 * @property integer $default_materials_0
 * @property integer $default_materials_1
 * @property integer $default_materials_2
 * @property integer $default_materials_3
 * @property string $metal_margin
 * @property string $stones_margin
 * @property string $labor_margin
 * @property string $customer_ref
 * @property integer $has_own_margins
 * @property integer $verified
 * @property string $group_id
 * @property integer $access_level
 * @property integer $is_deleted
 * @property string $last_login
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $nickname
 * @property string $skype
 * @property string $address
 * @property string $city
 * @property string $postcode
 * @property string $www
 * @property string $about
 * @property integer $newsletter_agreement
 * @property string $state
 * @property string $password write-only password
 *
 * @property UserData $userData
 * @property Order[] $orders
 * @property OrderWeightLog[] $orderWeightLogs
 * @property Product[] $products
 * @property ProductAlternateName[] $productAlternateNames
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
	var $password;
	

	const ROLE_USER = 10;
	const ROLE_ADMIN = 0;

	
	public static function getStatuses()
	{
		return [
			self::STATUS_ACTIVE => 'Aktywny',
			self::STATUS_DELETED => 'Zablokuj',
		];
	}
	
	public static function getGroups()
	{
		return [
			0 => 'Admin',
			10=>'Handlowiec',
		];
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public static function getFilterList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'name');
    }

    public function getName()
    {
        return $this->__toString();
    }


    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [[ 'email', 'group_id'], 'required'],
            [['status', 'created_at', 'updated_at', 'group_id'], 'integer'],
            [['password'], 'string'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'group_id' => Yii::t('app', 'Uprawnienia'),
            'discount' => Yii::t('app', 'Discount'),
            'modified_metal_price' => Yii::t('app', 'Modified Metal Price'),
            'default_materials_0' => Yii::t('app', 'Default Materials 0'),
            'default_materials_1' => Yii::t('app', 'Default Materials 1'),
            'default_materials_2' => Yii::t('app', 'Default Materials 2'),
            'default_materials_3' => Yii::t('app', 'Default Materials 3'),
            'metal_margin' => Yii::t('app', 'Metal Margin'),
            'stones_margin' => Yii::t('app', 'Stones Margin'),
            'labor_margin' => Yii::t('app', 'Labor Margin'),
            'customer_ref' => Yii::t('app', 'Customer Ref'),
            'has_own_margins' => Yii::t('app', 'Has Own Margins'),
            'verified' => Yii::t('app', 'Verified'),
            'access_level' => Yii::t('app', 'Access Level'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'last_login' => Yii::t('app', 'Last Login'),
            'first_name' => Yii::t('app', 'Imię'),
            'last_name' => Yii::t('app', 'Nazwisko'),
            'phone' => Yii::t('app', 'Phone'),
            'nickname' => Yii::t('app', 'Nickname'),
            'skype' => Yii::t('app', 'Skype'),
            'address' => Yii::t('app', 'Address'),
            'city' => Yii::t('app', 'City'),
            'postcode' => Yii::t('app', 'Postcode'),
            'www' => Yii::t('app', 'Www'),
            'about' => Yii::t('app', 'About'),
            'newsletter_agreement' => Yii::t('app', 'Newsletter Agreement'),
            'state' => Yii::t('app', 'State'),
			'password'=>Yii::t('app', 'Hasło'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserData()
    {
        return $this->hasOne(UserData::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

	public function beforeSave($insert)
    {
		if($this->password)
		{
			$this->generateAuthKey();
			$this->setPassword($this->password);
		}
		
		$this->username = $this->email;
		
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {

        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString().'_'.time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
