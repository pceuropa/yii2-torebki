<?php
namespace frontend\assets;
use yii\web\AssetBundle;

/**
 * Main app application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        #'http://fonts.googleapis.com/css?family=Raleway:300,400,500,700&amp;subset=latin-ext',
        'css/font-awesome.min.css',
        'css/yamm.css',
        'css/ekko-lightbox.min.css',
        'css/main.less',
    ];
    public $js = [               
        'js/ekko-lightbox.min.js',
        'js/jquery.blockUI.min.js',
        '/js/app.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
