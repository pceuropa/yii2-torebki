  <div class="container">
      <div class="row" style="padding-top: 40px; padding-bottom: 40px;">
          <div class="col-sm-4">
              <form action="/newsletter/subscribe" method="post">
                  <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                  <h4 class="beauty">Newsletter</h4>
                  <hr class="header-delimiter"/>
                  Dowiedz się o różnych ofertach i promocjach!
                  <div class="input-group">
                      <input type="text" class="form-control email-input" placeholder="Adres e-mail" name="NewsletterEmail[mail]"/>
                      <span class="input-group-btn">
                          <button class="btn btn-primary email-button" type="submit">Wyślij!</button>
                      </span>
                  </div>
                  <div class="checkbox">
                      <label><input type="checkbox" name="NewsletterEmail[terms]">Akceptuję regulamin</label>
                  </div>
              </form>
          </div>

          <div class="col-sm-8 our-adventages">
              <h4 class="beauty">Nasze atuty</h4>
              <hr class="header-delimiter"/>
              <div class="col-sm-4">
                  <span><img src="/images/fast_delivery.png"/></span>
                  <span class="caption"> <b style="display: block">Szybka<br>dostawa</b></span>
              </div>
              <div class="col-sm-4">
                  <span> <img src="/images/calendar.png"/></span>
                  <span class="caption"> <b style="display: block">14 dni <br>na zwrot</b></span>
              </div>
              <div class="col-sm-4">
                  <span> <img src="/images/secure_payments.png"/></span>
                  <span class="caption"> <b style="display: block">Bezpieczne <br>płatności</b></span>
              </div>
          </div>
      </div>
  </div>
