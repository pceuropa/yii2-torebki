<?php
/* @var $this View */
/* @var $content string */

use yii\helpers\{Url, Html};
use yii\web\View;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\NavBar;

use frontend\assets\AppAsset;
use common\models\Product;
use common\widgets\Alert;
use common\modules\cms\models\Box;
use common\modules\cms\models\Page;

$isHome = isset(Yii::$app->params['home']) ? true : false;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <?= $this->render('_head'); ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <header>
            <div class="text-center top-address hidden"> <?= Box::forceGet('top-address')->content; ?></div>
            <nav>
                <?= $this->render('_nav'); ?>
            </nav>
            <div class="container">
                <?= Alert::widget() ?>
            </div>
            <div class="header <?= !$isHome ? 'header-small' : ''; ?>">
                <?= $this->render('_navbar'); ?>

                <?php if ($isHome){ ?>
                    <?php echo $this->render('_slider'); ?>
                    <?php echo $this->render('_four_boxes'); ?>
                <?php } ?>
            </div>
        </header>

        <div class="container">
            <?= Breadcrumbs::widget([ 'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], ]) ?>
            <br />
            <?= $content ?>
        </div>

        <section class="about">
            <?= $this->render('_about'); ?>
        </section>

        <?php //echo $this->render('_logos');     ?>

        <div class="newsletter">
            <?= $this->render('_newsletter_section'); ?>
        </div>
        <?= $this->render('image_bar'); ?>

        <div class="line"></div>

        <div class="container">
            <div class="footer" style="padding-bottom: 20px; padding-top: 50px;">
                <?= $this->render('_footer'); ?>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-muted">
                    <?php if ($_SERVER['REQUEST_URI'] == '/') { ?>
                        <?= Box::forceGet('seo_text')->content; ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="clear20"></div>
        <div class="line"></div>

        <?= $this->render('_copyright'); ?>

    </body>
</html>
<?php
$this->endPage();
