<?php 
use yii\helpers\Url;
use common\models\Category;
?>
<form id="searchBar" class="search-bar-wrapper" method="get" action="<?= Url::to(['product/search']); ?>">
    <div class="search-bar">

        <div class="search-text">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
            <input type="text" autocomplete="off" value="" name="ProductSearch[name]" placeholder="Szukana fraza">
        </div>

        <div class="search-groups visible-lg visible-md">
            <select name="ProductSearch[category_id]">
                <option value="">wszystkie</option>
                <?php foreach (Category::getMain() as $category): ?>
                    <option value="<?= $category->id; ?>"><?= $category->title; ?></option>
                    <?php $childs = $category->getChildLimit(21)->all(); ?>
                    <?php foreach ($childs as $child): ?>
                        <option value="<?= $child->id; ?>"> -- <?= $child->title; ?></option>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="search-button">
            <button class="btn btn-success btn-orange" type="submit">
                <i class="fa fa-search" aria-hidden="true"></i>
            </button>
        </div>

    </div>
</form>
