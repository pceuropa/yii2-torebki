<?php

use common\models\Category;
use common\models\Basket;
use common\themes\sportstyle\assets\AppAsset;
use raoul2000\bootswatch\BootswatchAsset;
use yii\helpers\Html;

BootswatchAsset::$theme = 'readable';
AppAsset::register($this);
$baseUrl = $this->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <title><?= Html::encode($this->title) ?></title> 
        <?php $this->head() ?>

    </head>
    <body>
        <?php $this->beginBody() ?>
        <!-- Navigation -->
        <div class="container">

            <!-- Heading Row -->
            <div class="row">
                <div class="col-md-4">
                    <a href="/"><?= Html::img('@web/images/logo.jpg', ['class' => 'img-responsive logo']); ?></a>
                </div>
                <div class="col-md-4">

                    <form id="searchBar" class="search-bar-wrcommoner" method="get" action="/szukaj">
                        <div class="search-bar">
                            <div class="search-text">
                                <input type="hidden" name="<?= Yii::$common->request->csrfParam; ?>" value="<?= Yii::$common->request->csrfToken; ?>"/>
                                <input type="text" autocomplete="off" value="" name="ProductSearch[name]" placeholder="Szukana fraza">
                            </div>
                            <div class="search-groups visible-lg visible-md">
                                <select name="ProductSearch[category_id]">
                                    <option value="">wszystkie</option>
                                    <?php foreach (Category::getMain() as $category): ?>
                                        <option value="<?= $category->id; ?>"><?= $category->title; ?></option>
                                        <?php $childs = $category->getChildLimit(21)->all(); ?>
                                        <?php foreach ($childs as $child): ?>
                                            <option value="<?= $child->id; ?>"> -- <?= $child->title; ?></option>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="search-button">
                                <button class="button" type="submit"><i class="fontello icon-search"></i></button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-md-4">
                    <div class="bars">
                        <div class="phone menu-bars ">
                            + 48 32 270 24 15<br>
                            <a href="/pl/page/kontakt">Napisz do nas</a>
                        </div>
                        <div class="user text-center menu-bars">
                            <a href="/client/profile">
                                <?= Html::img('@web/images/user.png', ['class' => 'img-responsive']); ?>
                                <span>Twój profil</span>
                                <?php if (!Yii::$common->user->isGuest): ?>
                                    <?= Yii::$common->user->identity->toString ?>
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="cart text-center menu-bars">
                            <a href="/shop/card" id="basket-ico">
                                <?= Html::img('@web/images/cart.png', ['class' => 'img-responsive']); ?>
                                <?php if ($count = Basket::scount()): ?>
                                    <span class="badge badge-danger"><?= $count; ?></span>
                                <?php endif; ?>
                                <span>Koszyk</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="http://placehold.it/150x50&text=Logo" alt="">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">

            <!-- Heading Row -->
            <div class="row">
                <div class="col-md-8">
                    <img class="img-responsive img-rounded" src="http://placehold.it/900x350" alt="">
                </div>
                <!-- /.col-md-8 -->
                <div class="col-md-4">
                    <h1>Business Name or Tagline</h1>
                    <p>This is a template that is great for small businesses. It doesn't have too much fancy flare to it, but it makes a great use of the standard Bootstrap core components. Feel free to use this template for any project you want!</p>
                    <a class="btn btn-primary btn-lg" href="#">Call to Action!</a>
                </div>
                <!-- /.col-md-4 -->
            </div>
            <!-- /.row -->

            <hr>

            <!-- Call to Action Well -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="well text-center">
                        This is a well that is a great spot for a business tagline or phone number for easy access!
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <!-- Content Row -->
            <div class="row">
                <div class="col-md-4">
                    <h2>Heading 1</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor consectetur.</p>
                    <a class="btn btn-default" href="#">More Info</a>
                </div>
                <!-- /.col-md-4 -->
                <div class="col-md-4">
                    <h2>Heading 2</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor consectetur.</p>
                    <a class="btn btn-default" href="#">More Info</a>
                </div>
                <!-- /.col-md-4 -->
                <div class="col-md-4">
                    <h2>Heading 3</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor consectetur.</p>
                    <a class="btn btn-default" href="#">More Info</a>
                </div>
                <!-- /.col-md-4 -->
            </div>
            <!-- /.row -->

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>