<?php 

use yii\helpers\Html;
use common\models\Category;
?>
<div class="navbar yamm"><div class="container">

  <div class="navbar-header visible-xs visible-sm">
      <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
  </div>

  <div id="navbar-collapse-1" class="navbar-collapse collapse padding-0">
      <ul class="nav navbar-nav">
          <?php foreach (Category::getMain() as $category): ?>
<li>
<?= Html::a(
    Html::img('/images/category_' . $category->slug . '.png', ['class' => '']) .  $category->title,
    ['product/index', 'id' => $category->id, 'slug' => $category->slug],
    ['style' => 'todo', 'aria-expanded' => "false"])
?>
</li>
              <?php endforeach; ?>
      </ul>
  </div>

</div></div>
