<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\Basket;
?>
<div class="bars">

    <div class="phone menu-bars text-center">
        <a href="<?= Url::to(['site/contact']); ?>">
            <i class="glyphicon glyphicon-envelope text-gray" style="color: #B2B2B2; font-size: 24pt;" aria-hidden="true"></i>
            <br />
            <span style="color: #2D2D2D;">
                <?= Yii::t('app', 'Napisz do nas') ?>
            </span>
        </a>
    </div>

    <div class="user text-center menu-bars">
        <a href="<?= Url::to(['client/profile']); ?>">
            <?= Html::img('@web/images/user.png', ['class' => 'img-responsive']); ?>
            <span>Twój profil</span>
            <?php if (!Yii::$app->user->isGuest): ?>
                <?= Yii::$app->user->identity->toString ?>
            <?php endif; ?>
        </a>
    </div>

    <div class="cart text-center menu-bars">
        <a href="<?= Url::to(['shop/card']); ?>" id="basket-ico">
            <?= Html::img('@web/images/cart.png', ['class' => 'img-responsive']); ?>
            <?php if ($count = Basket::scount()): ?>
                <span class="badge badge-danger"><?= $count; ?></span>
            <?php endif; ?>
            <span>Koszyk</span>
        </a>
    </div>

</div>
