<?php

use yii\widgets\ListView;

?>

</div>
<div class="container">
    <!-- breadcrump -->
    <div class="breadcrumb">
        <a href="/">Strona Główna</a>
        &raquo;
        <span>Szukaj</span>
    </div>

        <div class="product-list">
            <div class="container-fluid">
                <div class="row">
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemOptions' => ['class' => 'view'],
                        'itemView' => '_view',
                        'layout' => "{items}<div class='clearfix'></div>{pager}",
                    ]) ?>
                </div>
            </div>
        </div>



</div>
<div>