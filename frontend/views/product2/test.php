<?php
use frontend\modules\cms\models\Box;
?>
<div class="container">
    <!-- breadcrump -->
    <div class="breadcrumb">


    </div>

    <div class="product-panel">
        <div class="row">
            <div class="col-sm-4">
                <!-- image preview - modal window -->
                <div id="myModal" class="modal">
                    <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>


            </div>


            <div class="col-sm-7 price-panel">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="old-price"><?= $model->basicPriceBrutto(); ?> PLN</div>
                        <div class="price"><?= $model->priceBrutto(); ?> <span>PLN</span></div>
                        <!--                        <div>Rata tylko 37,74 zł</div>-->
                    </div>
                    <div class="col-sm-6">
                        <form class="form-inline" action="/shop/add?id=<?= $model->id; ?>" method="post">
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
                            <div class="form-group">
                                <input type="text" value="1" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-default">Do koszyka</button>
                        </form>
                        <br>
                        Dostępność produktu: <span class="text-warning">tylko <?= $model->quantity; ?> sztuk</span>
                    </div>
                </div>
            </div>

            <div class="col-md-7 col-md-offset-5">
                <div class="row">
                    <div class="col-sm-6">
                        Koszty transportu: <b>15 zł</b><br>
                        Dostawa w ciągu: <b>3-5 dni roboczych</b><br>
                        Odbiór osobisty<b></b>
                    </div>
                    <div class="col-sm-6">
                        <!--                        <a href="#" style="color: #666666;">Dodaj do schowka</a><br>-->
                        <!--                        <a href="#" style="color: #666666;">Zapytaj o produkt</a>-->
                    </div>
                </div>

            </div>
        </div>

        <!-- tab menu -->
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#description">Opis i cechy</a>
                </li>
                <!--                <li>-->
                <!--                    <a data-toggle="tab" href="#reviews">Opinie</a>-->
                <!--                </li>-->
                <li>
                    <a data-toggle="tab" href="#guaranty">Gwarancja</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#accessories">Akcesoria do tego produktu</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#maintenance">Jak konserwujemy produkty</a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="description" class="tab-pane fade in active">
                    <p><?= $model->description; ?></p>
                    <table class="table table-condensed table-striped">
                        <?php foreach ($model->productAttributes as $attribute): ?>
                            <tr>
                                <td><?= $attribute->attributeName->name; ?></td>
                                <td><?= $attribute->value; ?> <?= $attribute->unit; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <div id="reviews" class="tab-pane fade">
                    <p>Opinie</p>
                </div>
                <div id="guaranty" class="tab-pane fade">
                    <?= $model->guarantee ? $model->guarantee->name : ''; ?>
                </div>
                <div id="accessories" class="tab-pane fade">
                    <p>Akcesoria</p>
                </div>
                <div id="maintenance" class="tab-pane fade">
                    <p><?= Box::forceGet('konserwacja')->content; ?></p>
                </div>
            </div>
        </div>

        <!--        <div class="buttons">-->
        <!--            <img src="/img/polec_znajomym.png" class="img-responsive"/>-->
        <!--            <a href="#">Poleć znajomym</a>-->
        <!---->
        <!--            <img src="/img/drukuj.png" class="img-responsive"/>-->
        <!--            <a href="#">Wersja do druku</a>-->
        <!---->
        <!--            <img src="/img/zglos_blad.png" class="img-responsive"/>-->
        <!--            <a href="#">Zgłoś błąd</a>-->
        <!--        </div>-->
        <?php /*
        <div class="promotion">
            <div class="title">Proponowane produkty</div>
            <div class="header-delimiter"></div>

            <div class="product-list">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <a href="#">
                                <img src="http://www.pngall.com/wp-content/uploads/2016/05/Tablet-Free-PNG-Image.png"
                                     class="img-responsive" style="width: 128px; height: 128px;"/>
                                <b>ZOTAC ZBOX RI531 PLUS</b>
                                <div class="price"><span>600</span> 533 zł</div>
                                <span style="color: gray; font-size: 11px;">Tablety</span>
                                <button class="add-to-cart-button" style="display: none;">Do koszyka</button>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a href="#">
                                <img src="http://www.pngall.com/wp-content/uploads/2016/05/Tablet-Free-PNG-Image.png"
                                     class="img-responsive" style="width: 128px; height: 128px;"/>
                                <b>ZOTAC ZBOX RI531 PLUS</b>
                                <div class="price"><span>600</span> 533 zł</div>
                                <span style="color: gray; font-size: 11px;">Tablety</span>
                                <button class="add-to-cart-button" style="display: none;">Do koszyka</button>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a href="#">
                                <img src="http://www.pngall.com/wp-content/uploads/2016/05/Tablet-Free-PNG-Image.png"
                                     class="img-responsive" style="width: 128px; height: 128px;"/>
                                <b>ZOTAC ZBOX RI531 PLUS</b>
                                <div class="price"><span>600</span> 533 zł</div>
                                <span style="color: gray; font-size: 11px;">Tablety</span>
                                <button class="add-to-cart-button" style="display: none;">Do koszyka</button>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a href="#">
                                <img src="http://www.pngall.com/wp-content/uploads/2016/05/Tablet-Free-PNG-Image.png"
                                     class="img-responsive" style="width: 128px; height: 128px;"/>
                                <b>ZOTAC ZBOX RI531 PLUS</b>
                                <div class="price"><span>600</span> 533 zł</div>
                                <span style="color: gray; font-size: 11px;">Tablety</span>
                                <button class="add-to-cart-button" style="display: none;">Do koszyka</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
*/ ?>
    </div>