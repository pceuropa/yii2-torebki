<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = Yii::t('app', 'Edytuj profil');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="text-right">
    <a href="/client/history" class="btn btn-success">Historia zamówień</a>
    <a href="/site/logout" data-method="post" class="btn btn-success">Wyloguj</a>
    </div>
    <h2 class="beauty"><span>Edytuj profil</span></h2>
    <div class="clear20"></div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <div class="clear20"></div>
</div>




