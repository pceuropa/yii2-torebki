<?php

use common\models\Client;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Inflector;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form box-body">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Dane właściciela konta</div>
                <div class="panel-body">
                    <?= $form->field($model, 'c_name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'c_postcode')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'c_city')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'c_address')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'c_country')->textInput(['maxlength' => true]) ?>                    
                    <?= $form->field($model, 'c_phone')->textInput(['maxlength' => true]) ?>
                    <div class="clear20"></div>
                    <?= $form->field($model, 'c_email')->textInput(['maxlength' => true]) ?>
                    <?php if ($model->scenario == Client::SCENARIO_REGISTER): ?>
                        <?= $form->field($model, 'passwordNew')->passwordInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'passwordConfirm')->passwordInput(['maxlength' => true]) ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= $form->field($model, 'fv')->checkbox(['label' => 'Faktura VAT']) ?>                  
                    <?php $this->registerJs('
                        $("#client-fv").click(function(){
                            $("#fvat").toggle();
                        }); 
                    '); ?>
                </div>
                <div class="panel-body" <?php
                if (!$model->fv) {
                    echo 'style="display: none"';
                }
                ?> id="fvat">
                     <?= $form->field($model, 'fvCopyFromClient')->checkbox(['label' => $model->getAttributeLabel('fvCopyFromClient')]) ?>
                     <?= $form->field($model, 'fvCopyFromDelivery')->checkbox(['label' => $model->getAttributeLabel('fvCopyFromDelivery')]) ?>
                     <?= $form->field($model, 'fv_nip')->textInput(['maxlength' => true]) ?>
                     <?= $form->field($model, 'fv_company')->textInput(['maxlength' => true]) ?>
                     <?= $form->field($model, 'fv_address')->textInput(['maxlength' => true]) ?>
                     <?= $form->field($model, 'fv_postcode')->textInput(['maxlength' => true]) ?>
                     <?= $form->field($model, 'fv_city')->textInput(['maxlength' => true]) ?>
                     <?= $form->field($model, 'fv_country')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dane do wysyłki
                    <?= $form->field($model, 'deliveryCopyFromClient')->checkbox(['label' => $model->getAttributeLabel('deliveryCopyFromClient')]) ?>
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'd_company')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'd_name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'd_address')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'd_postcode')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'd_city')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'd_country')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'd_phone')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'd_email')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'term1')->checkbox(['label' => 'Oświadczam, iż zapoznałem się z treścią <a href="/site/regulamin">Regulaminu</a> i akceptuję jego wszystkie postanowienia.*']) ?>
            <?= $form->field($model, 'term2')->checkbox(['label' => 'Wyrażam zgodę na przetwarzanie moich danych osobowych, w celu świadczenia usług drogą elektroniczną za pośrednictwem sklepu internetowego.*']) ?>
            <?= $form->field($model, 'newsletter')->checkbox(['label' => 'Wyrażam zgodę na przetwarzanie moich danych osobowych, w celu otrzymywania informacji handlowych, o których mowa w art. 10 ustawy z dn. 18.07.2002 o Świadczeniu usług drogą elektroniczną.']) ?>
        </div>
    </div>
    <div class="text-center">
        <?= Html::submitButton('Wyślij', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

