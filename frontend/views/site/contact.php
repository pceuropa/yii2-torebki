<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Kontakt');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Yii::t('app', 'Kontakt') ?></h1>

    <div class="row">
        <div class="col-lg-5 col-xs-12">
            <div class="row">
                <span style="font-size: medium; color: #000000;">
                    <strong>
                        <span style="font-size: 24pt;">Dział handlowy</span>
                    </strong>
                </span>
            </div>
            <div class="row">
                <span style="color: #000000; font-size: 14pt;">
                    Informacje handlowe, faktury, przesyłki
                </span>
            </div>
            <div class="row">
                E: 	sklep@tanietorebki.pl
                <br />
                T: 	+48 12 345 67 89
            </div>
        </div>
        <div class="col-lg-5 col-xs-12">
            <h4><?= Yii::t('app', 'Formularz kontaktowy') ?></h4>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

            <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Imię i nazwisko')])->label(false) ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'phone')->textInput([ 'placeholder' => Yii::t('app', 'Telefon')])->label(false) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'email')->textInput([ 'placeholder' => Yii::t('app', 'Email')])->label(false) ?>
                </div>
            </div>

            <?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => Yii::t('app', 'Treść')])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Wyślij'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>


    </div>

</div>
