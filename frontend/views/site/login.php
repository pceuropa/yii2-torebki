<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row">
        <div class="col-md-4 col-md-offset-2 text-center">
            <h2>Mam już konto</h2>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Zaloguj się', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
            </div>
            <div style="color:#999;margin:1em 0">
                <?= Html::a('zapomniałem hasła', ['site/request-password-reset']) ?>.
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4 col-md-offset-1 text-center">
            <h2>Nowy klient</h2>
            <br/>
            <hr/>
            <a href="/client/register" class="btn btn-success">Załóż konto</a>

            <br/>
            <hr/>
            <br/>
            <a href="/client/no-account" class="btn btn-success">Zamówienie bez zakładania konta</a>

        </div>

    </div>
</div>
