<?php
use common\models\ProductSearch;
use yii\helpers\Html;
?>
<h2 class="beauty pull-left">
    <span class="">
        <?php echo Yii::t('app', 'Polecane'); ?>    
    </span>  
</h2>

<span class="pull-right text-right">
    <?php echo Html::a(Yii::t('app', 'Pokaż wszystkie'), ['/'], ['class' => 'btn btn-default']) ?>    
</span>

<?php
$model = array_chunk(ProductSearch::byRecommend(10), 5);
foreach ($model as $level) {
    echo '<div class="clearfix margin-bottom-promo">';
    $i = 0;
    foreach ($level as $row) {
        $i++;
        echo $this->render('//product/_view', ['model' => $row, 'class' => 'col-xs-2 product-page-box text-center ' . ($i == 1 ? '' : 'col-half-offset')]);
    }
    echo '</div>';
}
?>

