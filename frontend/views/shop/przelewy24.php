
<h2 class="heading_page">
    <a href="#">Płatności online</a>
</h2>

<div style="padding:20px;">

    <form action="https://secure.przelewy24.pl/index.php" method="post" class="form" name="platnoscOnLine" id="platnoscOnLine">
        <div style="text-align:center">
            <input type="hidden" name="p24_session_id" value="<?= session_id(); ?>" />
            <input type="hidden" name="p24_id_sprzedawcy" value="<?= Yii::$app->setting->get('przelewy24_id'); ?>" />
            <input type="hidden" name="p24_kwota" value="<?= $model->toPay()*100; ?>" />
            <input type="hidden" name="p24_opis" value="Zamówienie #<?= $model->id; ?>" />
            <input type="hidden" name="p24_klient" value="<?= $model->c_name.' '.$model->c_surname; ?>" />
            <input type="hidden" name="p24_kraj" value="PL" />
            <input type="hidden" name="p24_email" value="<?= $model->c_email; ?>" />
            <input type="hidden" name="p24_language" value="pl" />
            <input type="hidden" name="p24_return_url_ok" value="http://<?= $_SERVER['HTTP_HOST']; ?>/shop/przelewy24" />
            <input type="hidden" name="p24_return_url_error" value="http://<?= $_SERVER['HTTP_HOST']; ?>/shop/przelewy24" />
            <button name="submit_send" type="submit" class="btn btn-success">
                PRZEJDŹ DO PŁATNOŚCI
            </button>
    </form>



    <script language="javascript">
        $(window).ready(function(){
            $('#platnoscOnLine').submit();
        })
    </script>

</div>


<!-- Google Code for Zakup Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 959991142;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "WjvQCNjpnl0Q5prhyQM";
    var google_conversion_value = 1.00;
    var google_conversion_currency = "PLN";
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/959991142/?value=1.00&amp;currency_code=PLN&amp;label=WjvQCNjpnl0Q5prhyQM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>