<strong>SKONFIGURUJ KOMPUTER</strong>

<div class="clear10"></div>
<?php foreach ($model->konfiguratorObjects() as $type => $k): ?>
<div class="row">

<div class="col-md-3"><label><?= $type; ?></label></div>
<div class="col-md-9">
    <select name="additional[]" class="form-control options">
        <option data-price="0">standard</option>
        <?php foreach ($k as $r): ?>
            <option value="<?= $r->id; ?>" data-price="<?= $r->price; ?>">
                <?= $r->value; ?> (+<?= $r->price; ?>)
            </option>
        <?php endforeach; ?>
    </select>
</div>
</div>

<div class="clear10"></div>
<?php endforeach; ?>
