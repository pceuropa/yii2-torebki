    <div style="height: 15px"></div>
    <li class="sidebar-header">
        <h4>Kategorie</h4>
        <hr class="sidebar-header-delimiter"/>
    </li>

    <?php if ($category->parentCategory and $category->parentCategory->parent != 0): ?>
        <li> <a href="<?= $category->parentCategory->getLink(); ?>" class="active">&raquo; <?= $category->parentCategory->title; ?></a> </li>
    <?php endif; ?>

    <?php foreach ($category->getChildren()->where(['active' => 1])->all() as $subcategory): ?>
        <li> <a href="<?= $subcategory->getLink(); ?>" class="active"><?= $subcategory->title; ?></a> </li>
    <?php endforeach; ?>
