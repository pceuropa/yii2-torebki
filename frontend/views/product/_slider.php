
<div class="text-center slider-product">
    <div onclick="window.location.href='<?= $model->url(); ?>'">

    <div class='max-width'>
        <?= $model->img(['class' => 'img-promo-time']); ?>
    </div>

    <p class='title'>
        <a class="title" href="<?= $model->url(); ?>"><?= $model->name; ?></a>
    </p>

        <p class="product-price">
            <?php if ($model->isPromo()): ?>
                <span><?= $model->basicPriceBrutto(); ?></span>
            <?php endif; ?>
            <?= $model->priceBrutto(); ?>
        </p>

        <div class="clearfix"></div>
        <div class="category">
            <?= $model->category->title; ?>
        </div>
    </div>
</div>
