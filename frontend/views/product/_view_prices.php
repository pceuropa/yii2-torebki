<?php 
   use yii\helpers\Url;
?>
<?php if ($model->priceBrutto() > 0 and $model->quantity > 0){ ?>

<form class="form-inline" action="<?= Url::to(['shop/add', 'id' => $model->id]) ?>" method="post">
  <div class="row">
      <div class="price-panel">

          <div class="col-sm-6">
               <?= $this->render('_view_prices_details', ['model' => $model]); ?>
          </div>

          <div class="col-sm-6">
               <?= $this->render('_view_add_to_cart', ['model' => $model]); ?>
          </div>

          <div class="clearfix"></div>
      </div>
  </div>

  <div class="col-sm-11 col-sm-offset-1">
      Koszty dostawy: <b>15  zł - po wpłacie na konto</b><br>
      Dostawa w ciągu: <b>3 dni roboczych</b>
  </div>
</form>

<?php } else { ?>

    <div class="row">
        <div class="col-md-122">
            <a href="/pl/page/kontakt" class="btn"><img src="/images/zapytaj.png"/></a>
            <br>
            Dostępność produktu: <span class="text-warning">tylko <?= $model->quantity; ?> sztuk</span>
        </div>
    </div>

<?php } ?>
