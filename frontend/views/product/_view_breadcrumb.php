<div class="breadcrumb">
<a href="/">Strona Główna</a> &raquo;

<?php if (method_exists($model, 'breadcrumps')) {

foreach ($model->breadcrumbs() as $bcategory){;?>
    <a href="<?= $bcategory->getLink() ?? '#'; ?>"><?= $bcategory->title; ?></a> &raquo;
<?php }
  
}?>


<a href="#"><?= $model->name; ?></a>
</div>
