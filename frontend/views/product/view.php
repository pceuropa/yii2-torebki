<?php
    use frontend\modules\cms\models\Box;
?>
<div class="container">
    <?= $this->render('_view_breadcrumb', ['model' => $model]); ?>
    <div class="product-panel">
        <div class="row">
            <div class="col-md-5">
               <?= $this->render('_view_images', [ 'model' => $model]); ?>
            </div>

            <div class="col-md-7">
               <?= $this->render('_view_title', [ 'model' => $model]); ?>
               <?= $this->render('_view_prices', [ 'model' => $model]); ?>
            </div>
        </div>
<br />
        <!-- tab menu -->
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"> <a data-toggle="tab" href="#description">Opis</a> </li>
                <li><a data-toggle="tab" href="#cechy">Specyfikacja</a> </li>
                <li><a data-toggle="tab" href="#guaranty">Gwarancja</a> </li>
            </ul>

            <div class="tab-content">

                <div id="description" class="tab-pane fade in active">
                    <p><?= $model->description; ?></p>
                </div>

                <div id="cechy" class="tab-pane fade">
                    <table class="table table-condensed table-striped">
                        <?php foreach ($model->productAttributesSort as $attribute): ?>
                            <tr> <td><?= $attribute->attributeName->name; ?></td> <td><?= $attribute->value; ?> <?= $attribute->unit; ?></td> </tr>
                        <?php endforeach; ?>
                    </table>
                </div>

                <div id="reviews" class="tab-pane fade">
                    <p>Opinie</p>
                </div>

                <div id="guaranty" class="tab-pane fade">
                    <?= $model->guarantee ? $model->guarantee->name : ''; ?>
                </div>

                <div id="accessories" class="tab-pane fade">
                    <p>Akcesoria</p>
                </div>

                <div id="maintenance" class="tab-pane fade"> <p><?= Box::forceGet('konserwacja')->content; ?></p>
                </div>
            </div>
        </div>

      <?php if ($suggested): ?>
         <?= $this->render('_promoted_products', ['suggested' => $suggested]); ?>
      <?php endif; ?>
</div>
