<div class="promotion">
<div class="title">Proponowane produkty</div>
<div class="header-delimiter"></div>

<div class="product-list">
  <div class="container-fluid">
      <div class="row">
          <?php foreach ($suggested as $product): ?>
              <div class="col-md-6 col-lg-3">
                  <a href="<?= $product->url(); ?>">
                      <?= $product->img(['style' => "width: 128px; height: 128px;", 'class' => "img-responsive"]); ?>
                      <b>
                          <?= mb_substr($product->name, 0, 28) . (mb_strlen($product->name) > 28 ? '...' : ''); ?>
                      </b>
                      <div class="price">
                          <?php if ($product->isPromo()): ?>
                              <span><?= $product->basicPriceBrutto(); ?></span>
                          <?php endif; ?>
                          <?= $product->priceBrutto(); ?> zł
                      </div>
                      <span style="color: gray; font-size: 11px;"><?= $product->category->title ?? ''; ?></span>
                  </a>
              </div>
          <?php endforeach; ?>

      </div>
  </div>
</div>
</div>
</div>
