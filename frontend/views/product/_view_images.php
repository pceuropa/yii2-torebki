<div id="myModal" class="modal">
    <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>

<div class="main-photo">
    <a href="<?= $model->getUrl('1024x600'); ?>" data-gallery="multiimages" data-toggle="lightbox" data-title="<?= $model->name; ?>">
        <img id="myImg" src="<?= $model->getUrl('1024x600'); ?>" class="img-responsive" alt="<?= $model->name; ?>">
    </a>
</div>

<div class="thumbnails">
    <?php foreach ($model->images as $image): ?>
        <a href="<?= $image->getUrl('1024x600'); ?>" data-gallery="multiimages" data-toggle="lightbox" data-title="<?= $model->name; ?>">
            <img id="myImg" src="<?= $image->getUrl('250x250'); ?>" width="64" class="img-responsive" alt="<?= $model->name; ?>">
        </a>
    <?php endforeach; ?>
</div>
