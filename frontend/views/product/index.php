<?php

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>

<div class="bg-category container">
        <!-- category description -->
        <div class="col-md-6 text-justify">
            <h3><?= $category->title ?></h3>
            <?= $category->description ?>
        </div>

        <div class="col-md-6">
            <?= $this->render('_sort'); ?>
        </div>
</div>

<div class="container">
    <div id="wrapper">
        <!-- left sidebar -->
        <?= $this->render('_left_sidebar', [ 'category' => $category, 'searchModel' => $searchModel, 'filters' => $filters]); ?>

        <!-- product list -->
        <div class="product-list">
                <div class="row">
                    <?php Pjax::begin([
                        'id' => 'pjax-product-list', 
                        'timeout' => 600, 
                        'enablePushState' => true, 
                        'enableReplaceState' => true, 
                        'clientOptions' => []
                    ]); ?>
                    <?= ListView::widget([
                        'id' => 'product-list',
                        'dataProvider' => $dataProvider,
                        'itemView' => '_view',
                        'layout' => "{items}<div class='col-md-12'>{pager}</div>",
                        'itemOptions' => ['class' => 'col-md-3 col-sm-12 product-page-box text-center']
                    ])
                    ?>
                    <?php Pjax::end(); ?>
            </div>
        </div>

    </div>
</div>
