
<?php if ($model->isPromo()){ ?>
    <span class="old-price"><?= $model->basicPriceBrutto(); ?> PLN</span>
     <span class='save-price'>Oszczędzasz: <?= $model->basicPriceBrutto() - $model->priceBrutto(); ?> PLN</span>
<?php } ?>


<div class="price mainprice" data-price="<?= $model->priceBrutto(); ?>">
    <?= $model->priceBrutto(); ?> <span>PLN</span><br/>
    <?php if (empty($model->getKonfigurator())): ?>
        <small class="text-muted" style="font-size:12px">NETTO: <?= $model->priceNetto(); ?> PLN</small>
    <?php endif; ?>
</div>

<?php 
if ($model->promotion_time) {?>

Koniec promocji:<br /><small id="promo_time"></small>


<?php $this->registerJs("
  var countDownDate = new Date('".$model->promotion_time."').getTime();

  var x = setInterval(function() {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById('promo_time').innerHTML = days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's ';

    if (distance < 0) {
        clearInterval(x);
        document.getElementById('demo').innerHTML = '';
    }
  }, 1000);
", 3);}
?>
<!--
<div class="btn" id="rata" onclick="PoliczRate(<?= $model->priceBrutto(); ?>);">Oblicz rate</div>
-->
