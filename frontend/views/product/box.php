<?php 
use yii\helpers\Html;
$anchor =  $model->name;
$slug = $model-> slug()
?>

<div class="col-md-3 product-page-box text-center">
   <?=  Html::a($model->img(), ['product/view', 'hash' => $slug, 'id' => $model->id], ['alt' => $model->name, 'title' => $model->name,  'class' => 'img-block', 'data-pjax' => 0]);  ?>
<hr>
   <?=  Html::a($anchor, ['product/view', 'hash' => $slug, 'id' => $model->id], [ 'alt' => $model->name, 'title' => $model->name, 'class' => 'title', 'data-pjax' => 0]);  ?>

        <p class="product-price">
            <?php if($model->isPromo()): ?>
               <span><?= $model->basicPriceBrutto(); ?></span>
            <?php endif; ?>
            <?= $model->priceBrutto(); ?>
        </p>

        <div class="clearfix"></div>
        <div class="category">
            <?= $model->category->title; ?>
        </div>
   <?= Html::a('Do koszyka', ['shop/add', 'id' => $model->id], [ 'class' => 'button']);  ?>
</div>
