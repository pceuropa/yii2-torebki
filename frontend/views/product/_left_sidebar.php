<?php 
use yii\helpers\Html;
?>
<div id="sidebar-wrapper" class="sidebar-filters">

        <ul class="sidebar">
            <?= $this->render('_category', ['category' => $category]); ?>

    <form method="get" action="" id="filter-form">
          <?= $this->render('_filter', ['category' => $category, 'searchModel' => $searchModel, 'filters' => $filters ]); ?>
          <?= $this->render('_price', ['searchModel' => $searchModel]); ?>
          <li class="remove-filters" style="padding: 0px 20px 20px 20px; text-align: right"> </li>
     </ul>
    </form>
<br/>
</div>
