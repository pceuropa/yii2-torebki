<?php
use yii\widgets\ListView;
?>

</div>
<div class="container">
    <div class="breadcrumb"> <a href="/">Strona Główna</a> &raquo; <span>Szukaj</span> </div>

    <div class="product-list">
        <div class="container-fluid">
            <div class="row">
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'col-md-3 col-sm-12 product-page-box text-center'],
                    'itemView' => '_view',
                    'layout' => "{items}<div class='clearfix'></div>{pager}",
                ]) ?>
            </div>
        </div>
    </div>
</div>
<div>
