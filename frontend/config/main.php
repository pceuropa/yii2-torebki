<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
	'homeUrl' => '/',
    'language' => 'pl',
    'sourceLanguage'=>'en',
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'components' => [

        'request' => [
            'csrfParam' => '_csrf-frontend',
			'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'user' => [
            'identityClass' => 'common\models\Client',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],

        'setting' => [
            'class' => 'common\components\Setting',
        ],
        'session' => [
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/newsletter/confirm/<email:.+>/<hash:.+>' => 'newsletter/confirm',
                '/pl/page/<link:.+>' => 'cms/page/view',
                '/produkt/<hash:.+>-<id:.+>' => 'product/view',
                '/produkty/<slug:.+>-<id:.+>' => 'product/index',
                '/szukaj' => 'product/search',

                '/produkty/<hash:.+>-<id:.+>'=>'product/index',
                '/produkt/<hash:.+>-<id:.+>'=>'product/view',

				'shop/add/<id:\d+>' => 'shop/add',
				'shop/delete/<key:\d+>' => 'shop/delete',

				'news/' => 'news/index',
				'news/<seotitle:[\w\-]+>-n<id:\d+>' => 'news/view',
				'category/' => 'category/index',
				'category/<seotitle:[\w\-]+>-c<id:\d+>' => 'category/view',
				'tags/<tag:[\w\-]+>' => 'tags/index',
            ],
        ],
    ],
    'params' => $params,
    'modules' => [
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            'imagesStorePath' => '@app/web/images/store', //path to origin images
            'imagesCachePath' => '@app/web/images/cache', //path to resized copies
            'graphicsLibrary' => 'Imagick', //but really its better to use 'Imagick'
        ],
        'cms' => [
            'class' => 'frontend\modules\cms\Cms',
        ],
    ],
];
