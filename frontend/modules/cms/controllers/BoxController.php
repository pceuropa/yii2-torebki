<?php

namespace frontend\modules\cms\controllers;

use frontend\modules\cms\Cms;
use common\widgets\Alert;
use Yii;
use frontend\modules\cms\models\Box;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BoxController extends Controller
{



    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Box();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Alert::add(Cms::t('msg', 'Operation completed successfully.'), Alert::TYPE_SUCCESS);

        return $this->redirect(['index']);
    }

    /**
     * @param string $id
     * @return Box the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Box::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
