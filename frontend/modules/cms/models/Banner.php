<?php

namespace frontend\modules\cms\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%banners}}".
 *
 * @property string $id
 * @property string $title
 * @property string $link
 * @property string $photo
 * @property integer $active
 * @property string $language
 */
class Banner extends ActiveRecord
{
    var $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_banner}}';
    }

    public static function getList()
    {
        return Banner::find()->where(['active' => 1])->orderBy('position asc')->all();
    }


}
