<?php

namespace frontend\models;

use common\models\Delivery;
use common\models\Product;
use common\models\ShopConfigurator;
use common\models\VoucherCode;
use Symfony\Component\DependencyInjection\Loader\IniFileLoader;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Basket extends Model {

    private $_basket;
    public $error = [];

    const PAYMENT_PRZELEW = 'przelew';
    const PAYMENT_ONLINE = 'online';
    const PAYMENT_ODBIOR = 'odbior';
    const PAYMENT_RATY = 'raty';

    public static $payments = [
        self::PAYMENT_PRZELEW => 'Przelew',
        self::PAYMENT_ONLINE => 'Płatność online',
        self::PAYMENT_ODBIOR => 'Płatność przy odbiorze',
        self::PAYMENT_RATY => 'Raty żagiel'
    ];

    public static function scount() {
        $basket = new self();
        return $basket->count();
    }

    public function init() {

        if (Yii::$app->session->has('basket')) {
            $this->_basket = unserialize(Yii::$app->session->get('basket'));
        }
    }

    public function count() {
        if ($this->isEmpty()) {
            return 0;
        } else {
            return count($this->_basket['items']);
        }
    }

    public function isEmpty() {
        return empty($this->_basket['items']);
    }

    public function getItems() {
        foreach ($this->_basket['items'] ?: [] as $item) {
            $add = 0;
            if (isset($item['options'])) {
                foreach ($item['options'] as $o) {
                    $add += $o['price'];
                }
            }
            yield ['product' => Product::findOne($item['product_id']), 'attributes' => $item, 'add_price' => $add];
        }
    }

    public function add($id) {
        $product = Product::findOne($id);
        if (!$product->price or ! $product->quantity) {
            $this->error[] = 'Wybrany produkt jest niedostępny';
            return ['link' => $product->url()];
        }

        $q = 1;
        if (isset($_POST['q']) and is_numeric($_POST['q']) and $_POST['q'] > 0) {
            $q = $_POST['q'];
        }

        if ($q > $product->quantity) {
            $this->error[] = 'Nie wystarczająca ilość produktów na stanie';
            return ['link' => $product->url()];
        }

        $options = [];
        if (isset($_POST['additional'])) {
            foreach ($_POST['additional'] as $a) {
                if ($tmp = ShopConfigurator::findOne([$a])) {
                    $tmp->product();
                    $options[] = [
                        'option_id' => $a,
                        'name' => $tmp->value,
                        'product_id' => $tmp->product_id,
                        'price' => $tmp->price,
                    ];
                }
            }
        }

        $this->_basket['items'][] = ['product_id' => $id, 'quantity' => $q, 'options' => $options];

        $this->save();
        return false;
    }

    public function delete($id) {
        unset($this->_basket['items'][$id]);
        $this->save();
    }

    public function change($key, $quantity) {
        $this->_basket['items'][$key]['quantity'] = $quantity;
        $this->save();
    }

    public function productsPrice() {
        $sum = 0;
        foreach ($this->getItems() as $item) {

            $sum += $item['attributes']['quantity'] * ($item['product']->priceBrutto() + $item['add_price']);
        }
        return $sum;
    }

    public function isVoucher() {
        return isset($this->_basket['voucher']) && $this->_basket['voucher'];
    }

    public function toPay() {
        $sum = $this->productsPrice();

        if ($this->isVoucher()) {
            if ($this->_basket['voucher_price']) {
                $sum -= $this->_basket['voucher_price'];
                if ($sum < 0) {
                    $sum = 0;
                }
            } elseif ($this->_basket['voucher_percent']) {
                $sum = $sum - ($sum * $this->_basket['voucher_percent'] / 100);
            }
        }

        $sum += $this->getTransportCost();

        return sprintf("%.2f", $sum);
    }

    private function save() {
        Yii::$app->session->set('basket', serialize($this->_basket));
    }

    public function remove() {
        Yii::$app->session->remove('basket');
    }

    public function getVoucherPrice() {
        return $this->_basket['voucher_price'];
    }

    public function getVoucherPercent() {
        return $this->_basket['voucher_percent'];
    }

    public function getVoucherCode() {
        return $this->_basket['voucher'];
    }

    public function setTransport($id) {
        //$transport = Delivery::findOne($id);
        $this->_basket['transportId'] = $id;
        $this->save();
    }

    public function getTransportId() {
        if (isset($this->_basket['transportId'])) {
            return $this->_basket['transportId'];
        } else {
            return null;
        }
    }

    public function getTransportCost() {
        if (isset($this->_basket['transportId'])) {
            $delivery = Delivery::findOne($this->_basket['transportId']);
            if (null != $delivery && $delivery->free && ($this->productsPrice() > $delivery->free)) {
                return 0;
            } elseif (null != $delivery && $delivery->free) {
                return $delivery->price;
            }
        } else {
            return 0;
        }
    }

    public function getTransportName() {
        if (isset($this->_basket['transportId'])) {
            $delivery = Delivery::findOne($this->_basket['transportId']);
            return $delivery->name;
        } else {
            return '';
        }
    }

    public function setClient($client) {
        $this->_basket['client'] = $client;
        $this->save();
    }

    public function getClient() {
        return $this->_basket['client'];
    }

    public function setPayment($id) {
        //$transport = Delivery::findOne($id);
        $this->_basket['paymentId'] = $id;
        $this->save();
    }

    public function getPaymentId() {
        if (isset($this->_basket['paymentId'])) {
            return $this->_basket['paymentId'];
        } else {
            return null;
        }
    }

    public function setNotice($notice) {
        $this->_basket['notice'] = $notice;
        $this->save();
    }

    public function getNotice() {
        if (isset($this->_basket['notice'])) {
            return $this->_basket['notice'];
        } else {
            return '';
        }
    }

    public function setVoucher($v) {
        $v = strtoupper($v);
        $this->_basket['voucher_percent'] = 0;
        $this->_basket['voucher_price'] = 0;
        $this->_basket['voucher'] = false;
        $v = VoucherCode::find()->where(['code' => $v])->limit(1)->one();


        if ($v) {
            if ($v->order_id) {
                $this->error[] = 'Podany voucher został już wykorzystany';
            } elseif (strtotime($v->voucher->date_from) > time() or strtotime($v->voucher->date_to . ' 23:59:59') < time()) {
                $this->error[] = 'Kod promocyjny obowiązuje<br/>od: ' . $v->voucher->date_from . ' do ' . $v->voucher->date_to;
            } else {
                $this->_basket['voucher'] = $v->code;
                if ($v->voucher->percent) {
                    $this->_basket['voucher_percent'] = sprintf("%.2f", $v->voucher->percent);
                }
                if ($v->voucher->price) {
                    $this->_basket['voucher_price'] = sprintf("%.2f", $v->voucher->price);
                }
            }
        } else {
            $this->error[] = 'Podany kod nie istnieje lub został błędnie wpisany. Wpisz jeszcze raz lub skontaktuj się z obsługą sklepu w celu ustalenia przyczyny';
        }

        $this->save();

        if ($this->error) {
            return false;
        } else {
            return true;
        }
    }

    public function noClient() {
        return empty($this->_basket['client']);
    }

}
