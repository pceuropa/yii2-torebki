<b>Zamówienie nr <?= $model->id; ?> złożone dnia: <?php echo date("d-m-Y") ?> <br/>- potwierdzenie wpłynięcia do sklepu</b>
<br/><br/>
Szanowny Kliencie,<br/>

do sklepu internetowego  wpłynęło zamówienie o numerze <?php echo $model->id; ?>.
<br/><br/>
Dziękujemy za zainteresowanie zakupami w naszym Sklepie. <br/>
O zmianie statusu zamówienia zostaną Państwo powiadomieni e-mailem lub telefonicznie.

<br/><br/>
Aktualny status realizacji zamówienia jest dostępny na <a href="http://www.tanietorebki.com.pl/client/history" >Twoim koncie. </a>
<br/><br/>
<b style="font-size:13px;">Szczegóły zamówienia:</b><br/><br/>

<table cellspacing="0" cellpadding="0" border="0" width="460">
    <tbody>
    <tr>
        <td width="144" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:11px;color:#444">Nazwa produktu</td>
        <td align="center" width="40" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:11px;color:#444">Ilość</td>
        <td align="right"  valign="top"  width="108" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:11px;color:#444">Cena netto</td>
        <td align="right"  valign="top"  width="108" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:11px;color:#444">Cena brutto</td>
        <td align="right" width="60" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:11px;color:#444">Wartość</td>
    </tr>
    <?php foreach ($model->items as $i): ?>
        <tr>
            <td valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444;border-top:1px solid #cccccc">
                <br>
                <b><?php echo $i->name; ?></b>
                <?php if ($i->options):
                    $a = json_decode($i->options);
                    foreach($a as $o)
                    echo '<br/>' . $o->name;
                 endif; ?>
                <br>
                <br>
                <br>
            </td>
            <td align="center" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444;border-top:1px solid #cccccc">
                <br>
                <?php echo $i->quantity; ?></td>
            <td align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444;border-top:1px solid #cccccc">
                <br>
                <?php echo sprintf("%.2f", $i->price_one ); ?> PLN
            </td>
            <td align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444;border-top:1px solid #cccccc">
                <br>
                <?php echo $i->price_one; ?> PLN</td>
            <td align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444;border-top:1px solid #cccccc">
                <br>
                <?php echo sprintf("%.2f", $i->quantity * $i->price_one); ?> PLN</td>
        </tr>
    <?php endforeach; ?>

    <tr>
        <td height="20" valign="top" colspan="4" style="border-top:1px solid #cccccc">&nbsp;</td>
    </tr>
    <tr>
        <td height="20" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444;font-weight:bold" >Wartość produktów:</td>
        <td height="20" align="left" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444" colspan="2"></td>
        <td height="20" align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444"><?php echo $model->products_price; ?> PLN</td>
    </tr>
    <?php if ($model->voucher_percent > 0 or $model->voucher_price > 0): ?>
        <tr>
            <td height="20" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444;font-weight:bold" >Rabat:</td>
            <td height="20" align="left" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444"></td>
            <td height="20" align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444"><?php echo $model->voucher_name; ?></td>
            <td height="20" align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444">
                <?php if($model->voucher_percent > 0) echo '-'.$model->voucher_percent.'%'; ?>
                <?php if($model->voucher_price > 0) echo '-'.$model->voucher_price.' PLN'; ?>
            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <td height="20" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444" >Przesyłka</td>
        <td height="20" align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444" colspan="2"><?php echo $model->transport_name; ?></td>
        <td height="20" align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444"><?php echo $model->transport_price; ?> PLN</td>
    </tr>
    <tr>
        <td height="20" align="left" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444" >Do zapłaty</td>
        <td height="20" align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444" colspan="2"><?php echo $model->payment_name; ?></td>
        <td height="20" align="right" valign="top" style="font-family:Arial,Geneva,Helvetica,sans-serif;font-size:12px;color:#444"><?php echo $model->toPay(); ?> PLN</td>
    </tr>


    </tbody>
</table>

<br/>
<?php if ($model->fv): ?>

    <b style="font-size:13px;">Dane do Faktury VAT</b>
    <br/><br/>
    <?php echo $model->fv_company . ', ' . $model->fv_nip . ', ' . $model->fv_address . ', ' . $model->fv_postcode . ', ' . $model->fv_city . ', ' . $model->fv_country; ?>
    <br/><br/>
<?php endif; ?>




<b style="font-size:13px;">Adres wysyłki</b>
<?php
if ($model->d_company)
    echo $model->d_company . ', ';
echo $model->d_name . ', ' . $model->d_address . ', ' . $model->d_postcode . ', ' . $model->d_city . ', ' . $model->d_country;
?>
<br/><br/>

<br/><br/>

Pozdrawiamy,<br/>
Obsługa Sklepu Internetowego<br/>
<br/><br/>
PS: Jeżeli zamówienie zostało złożone przez inną osobę, prosimy o odesłanie tej wiadomości na adres sklep@tanietorebki.com.pl
