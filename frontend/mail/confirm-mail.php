<div style="width:460px; padding:20px;   color: #5e5e5e; border:1px solid #b6b6b6; font-size:9pt;">
    <div style="font-size:12pt; font-weight:bold; margin-bottom:10px;color:#000;">Obsługa klienta (<a href="mailto:sklep@tanietorebki.com.pl">sklep@tanietorebki.com.pl</a>)</div>

    Prosimy nie odpowiadać na tą wiadomość. Została wygenerowana automatycznie.<br/><br/>

    Dziękujemy za zarejestrowanie konta w naszym sklepie.<br/>
    Poniżej znajdują się Twoje dane do logowania do panelu Klienta:<br/><br/>

    Twój login: <?= $model->c_email; ?><br/>

    Aby się zalogować wejdź w poniższy adres:<br/>
    <a href="http://<?= $_SERVER['HTTP_HOST'] ?>/client/profile" >http://<?= $_SERVER['HTTP_HOST'] ?>/client/profile</a>

    <br/><br/>
    Aby sprawnie dokonywać zakupów, koniecznie uzupełnij swoje informacje dodatkowe w panelu klienta.<br/><br/>

    Życzymy udanych zakupów.<br/><br/>

    Jeśli to nie Ty rejestrowałeś się w naszym sklepie, możliwe, że ktoś omyłkowo wpisał Twój adres e-mail. W takiej sytuacji prosimy zignorować tą wiadomość..
    <br/>
</div>
