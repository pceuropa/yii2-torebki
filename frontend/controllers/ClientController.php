<?php

namespace frontend\controllers;

use common\models\Order;
use common\models\OrderSearch;
use frontend\models\Basket;
use Yii;
use common\models\Client;
use backend\models\ClientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['register', 'remind', 'no-account'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['profile', 'history', 'order'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Client model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister() {
        $model = new Client();
        $model->c_country = 'POLSKA';
        $model->scenario = Client::SCENARIO_REGISTER;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword($model->passwordNew);
            $model->generateAuthKey();
            $model->save(false);
            $model->sendConfirmMail();
            Yii::$app->getSession()->setFlash('success', '<h4>Konto zostało założone</h4>'
                    . '<p>W celu weryfikacji konta wyślemy powitalną wiadomość na poniższy adres'
                    . 'email, który podałeś przy tworzeniu konta:</p>'
                    . '<p>' . $model->c_email . '</p>'
                    . '<p><strong>Proszę sprawdź e-mail.</strong></p>'
                    . '<p>Jeżeli dane wprowadzone są błędne prosimy skontaktuj się z nami telefonicznie lub pod adresem e-mail: <a href="mailto:sklep@tanietorebki.com.pl">sklep@tanietorebki.com.pl</a></p>');
            return $this->redirect(['/site/login']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionNoAccount() {
        $model = new Client();

        $model->scenario = Client::SCENARIO_NO_ACCOUNT;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $basket = new Basket();
            $basket->setClient($model->attributes);
            Yii::$app->getSession()->setFlash('success', 'W celu weryfikacji zamówienia wyślemy podsumowanie na poniższy adres e-mail, który podałeś przy tworzeniu konta: '.$model->c_email                                
                    . '<p><strong>Proszę sprawdź e-mail.</strong></p>'
                    . '<p>Jeżeli dane wprowadzone są błędne prosimy skontaktuj się z nami telefonicznie lub pod adresem e-mail: <a href="mailto:sklep@tanietorebki.com.pl">sklep@tanietorebki.com.pl</a></p>');

            // $model->save(false);
            return $this->redirect(['shop/confirm']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionProfile() {

        $model = Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Zmiany zostały zapisane');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionHistory() {
        $searchModel = new OrderSearch();

        $searchModel->client_id = Yii::$app->user->identity->getId();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('history', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrder($id) {
        $model = $this->findOrder($id);
        return $this->render('order', ['model' => $model]);
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findOrder($id) {
        if (($model = Order::find()->where(['id' => $id, 'client_id' => Yii::$app->user->identity->getId()])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Autocomplete
     * @param string $term
     * @return JSON
     */
    public function actionAutocomplete($term) {
        $items = [];
        if ($list = Client::find()->andFilterWhere(['like', 'id', $term])->asArray()->all()) {
            foreach ($list as $l) {
                $items[] = ['label' => $l['id'], 'id' => $l['id'], 'value' => $l['id']];
            }
        }

        Yii::$app->response->format = 'json';
        return $items;
    }

}
