<?php

namespace frontend\controllers;

use Yii;
use yii\web\{Controller, NotFoundHttpException};
use yii\filters\{AccessControl, VerbFilter};
use common\models\{Product, Category, ShopAttribute, ShopProductAttribute};
use frontend\models\ProductSearch;

/** * ProductController implements the CRUD actions for Product model.  */
class ProductController extends Controller {

    /**
     * Lists all Products.
     * @return mixed
     */

    
    public function actionIndex($id){

        if (!$category = Category::findOne($id)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $searchModel = new ProductSearch();
        $ids = Category::getIdsWithSubcategories($id);

        if (!$filters = ShopProductAttribute::getCategoryFilters2($ids)) {
            $filters = ShopProductAttribute::getCategoryFilters2($ids, -1);
        } 

        $searchModel->category_id = $ids;


        return $this->render( 'index', [
            'searchModel' => $searchModel,
            'dataProvider' => $searchModel->search( Yii::$app->request->queryParams ),
            'category' => $category,
            'filters'=> $filters
        ] );
    }

    /**
     * Displays a single Product.
     * @param int $id
     * @return mixed
     */
    public function actionView($id, $hash){

        $product = $this->findModel($id);
        $suggested_products = Product::find()->where(['category_id'=>$product->category_id])->orderBy('rand()')->limit(4)->all();

        return $this->render('view', [
            'model' => $product,
            'suggested'=>$suggested_products,
        ]);
    }

    public function actionSearch() {

        $searchModel = new ProductSearch();
        $this->enableCsrfValidation = false;
        $dataProvider = $searchModel->searchWord(Yii::$app->request->get());

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Autocomplete
     * @param string $term
     * @return JSON
     */
    public function actionAutocomplete($term) {

        $items = [];
        if ($list  = Product::find()->andFilterWhere(['like', 'name', $term])->asArray()->all()) {
            foreach ($list as $l) {
                $items[] = ['label' => $l['name'], 'id' => $l['id'], 'value' => $l['name']];
            }
        }

        Yii::$app->response->format = 'json';
        return $items;
    }
}
